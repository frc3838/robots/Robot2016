package frc.team3838.testers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.utils.logging.LogbackProgrammaticConfiguration;



public class PovCheckRobot extends SampleRobot
{
    private static final Logger logger = LoggerFactory.getLogger(PovCheckRobot.class);


    Joystick joystick = new Joystick(2);


    @Override
    public void operatorControl()
    {
        testPov();
    }


    @Override
    public void test()
    {
        testPov();
    }

    private void testPov()
    {
        if (LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE)
        {
            SmartDashboard.putNumber("POV Reading: ", joystick.getPOV());
        }
    }
}
