/**
 * <strong style='color:red'>Subsystem classes MUST be singletons.</strong>
 * As such, they should have a private constructor and a public static
 * {@code getInstance()} method Additionally, any exceptions in the
 * constructor must be caught in the constructor &Amp; logged at the error level,
 * and the subsystem should be disabled in the {@link frc.team3838.config.Enablings} class.
 */
package frc.team3838.subsystems;