package frc.team3838.subsystems;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.reflections.ReflectionUtils;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team3838.commands.testing.DioScanTestCommand;
import frc.team3838.config.DIOs;
import frc.team3838.config.Enablings;

import static org.reflections.ReflectionUtils.withTypeAssignableTo;



public class DioTestingSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DioTestingSubsystem.class);


    private static int capacity = 33;
    private static final Map<Integer, DigitalInput> dioMap = new HashMap<>(capacity);



    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     DioTestingSubsystem subsystem = new DioTestingSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DioTestingSubsystem subsystem = DioTestingSubsystem.getInstance();
     * </pre>
     */
    private DioTestingSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());


                @SuppressWarnings("unchecked")
                final Set<Field> dioFields = ReflectionUtils.getFields(DIOs.class, withTypeAssignableTo(int.class));
                final SortedSet<Integer> assignedDios = new TreeSet<>();
                for (Field dioField : dioFields)
                {
                    try
                    {
                        dioField.setAccessible(true);
                        int usedDio = (int) dioField.get(null);
                        if (usedDio >= 0)
                        {
                            assignedDios.add(usedDio);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.warn("Exception when reflectively getting in use DIOs from DIOs class. Cause Summary: {}", e.toString());
                    }
                }

                logger.info("Pre Assigned DIOs: {}", assignedDios);


                for (int i = 0; i < capacity; i++)
                {
                    try
                    {
                        if (!assignedDios.contains(i))
                        {
                            DigitalInput dio = new DigitalInput(i);
                            dioMap.put(i, dio);
                            logger.info("DI {} initialized", i);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.info("Could not init DI {}. Cause: {}", i, e.toString());
                    }
                }


                logger.debug("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                Enablings.isDioTesterSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }


    @Override
    public void initDefaultCommand()
    {
        if (isEnabled())
        {
            setDefaultCommand(new DioScanTestCommand());
        }
    }


    public static boolean isEnabled()
    {
        return Enablings.isDioTesterSubsystemEnabled;
    }

    public void scan()
    {
        for (Entry<Integer, DigitalInput> entry : dioMap.entrySet())
        {
            final DigitalInput digitalInput = entry.getValue();
            if (!digitalInput.get())
            {
                logger.info("DIO {} Activated", entry.getKey());
            }
            else
            {
                logger.trace("DIO {} Off", entry.getKey());
            }
        }
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FIELD NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final DioTestingSubsystem singleton = new DioTestingSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new DioTestingSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     DioTestingSubsystem subsystem = new DioTestingSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DioTestingSubsystem subsystem = DioTestingSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static DioTestingSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/

}
