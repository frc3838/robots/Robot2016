# Subsystem classes MUST be singleton classes.
Subsystem classes MUST be singletons.
As such, they should have:

 - a private constructor
 - a `public static getInstance()` method
 - any exceptions in the constructor must be caught in the constructor & logged at the error level, and the subsystem  disabled in the `frc.team3838.config.Enablings` class.