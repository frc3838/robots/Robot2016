package frc.team3838.subsystems;


import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.config.Enablings;
import frc.team3838.config.PWMs;
import frc.team3838.utils.logging.LogbackProgrammaticConfiguration;
import frc.team3838.utils.motors.MotorOps;



public class WinchSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WinchSubsystem.class);

    private SpeedController hookExtensionSpeedController; //Spark
    private MotorOps hookExtensionMotorOps;
    private SpeedController winchLiftSpeedController; //TalonSR
    private MotorOps winchLiftMotorOps;

    // Modified 10/13/16 by KMS.  Started with 0.36.
    // Modified 10/18/16 by KMS.  0.18 is too slow.
    // 0.27 stalled about half way up.
    // set back to 0.36 and it works but it starts and stops fast.
    private static double hookRaiseSpeed = 0.36;  // started at 0.36
    private static double hookLowerSpeed = 0.28;
    // Modified 10/13/16 by KMS.  Started at 0.50
    private static double winchRaiseSpeed = 0.99; // started at 0.50
    private static double winchLowerSpeed = 0.50;

    private boolean inManualMode = false;

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     WinchSubsystem subsystem = new WinchSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     WinchSubsystem subsystem = WinchSubsystem.getInstance();
     * </pre>
     */
    private WinchSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());

                hookExtensionSpeedController = new Spark(PWMs.HOOK_EXTENSION_MOTOR);
                hookExtensionMotorOps = new MotorOps(hookExtensionSpeedController, true);
                winchLiftSpeedController = new Talon(PWMs.WINCH_LIFT_MOTOR);
                winchLiftMotorOps = new MotorOps(winchLiftSpeedController, false);

                logger.debug("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                Enablings.isWinchSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }

    public void hookExtend()
    {
        if (isEnabled())
        {
            try
            {
                logger.trace("hookExtend called hookRaiseSpeed = {}", hookRaiseSpeed);
                hookExtensionMotorOps.setForwardSpeed(hookRaiseSpeed);
            }
            catch (Exception e)
            {
                logger.warn("An exception occurred in hookExtend. Cause Summary: {}", e.toString(), e);
            }
        }
    }

    public void hookRetract()
    {
        if (isEnabled())
        {
            try
            {
                logger.trace("hookRetract() called hookLowerSpeed = {}", hookLowerSpeed);
                hookExtensionMotorOps.setReverseSpeed(hookLowerSpeed);
            }
            catch (Exception e)
            {
                logger.warn("An exception occurred in hookRetract. Cause Summary: {}", e.toString(), e);
            }
        }
    }

    public void stopHookMotor()
    {
        try
        {
            logger.trace("stopHookMotor called");
            hookExtensionMotorOps.stop();
        }
        catch (Exception e)
        {
            logger.warn("An exception occurred in stopHookMotor. Cause Summary: {}", e.toString(), e);
        }
    }


    /**
     * Extends (or slackens) the power winch (i.e. lets it out) which will lower the robot to the ground.
     */
    public void winchExtendSlashLetOutToLowerRobot()
    {
        if (isEnabled())
        {
            try
            {
                logger.trace("winchExtendSlashLetOutToLowerRobot() called winchRaiseSpeed = {}", winchRaiseSpeed);
                winchLiftMotorOps.setForwardSpeed(winchRaiseSpeed);
            }
            catch (Exception e)
            {
                logger.warn("An exception occurred in winchExtendSlashLetOutToLowerRobot. Cause Summary: {}", e.toString(), e);
            }
        }
    }


    /**
     * Retracts the power winch (i.e. pulls it in) which will lift the robot off the ground.
     */
    public void winchRetractSlashPullInToRaiseRobot()
    {
        if (isEnabled())
        {
            try
            {
                logger.trace("winchRetractSlashPullInToRaiseRobot() called winchLowerSpeed = {}", winchLowerSpeed);
                winchLiftMotorOps.setReverseSpeed(winchLowerSpeed);
            }
            catch (Exception e)
            {
                logger.warn("An exception occurred in winchRetractSlashPullInToRaiseRobot. Cause Summary: {}", e.toString(), e);
            }
        }
    }

    public void stopWinchMotor()
    {
        try
        {
            logger.trace("stopWinchMotor() caleed");
            winchLiftMotorOps.stop();
        }
        catch (Exception e)
        {
            logger.warn("An exception occurred in stopWinchMotor. Cause Summary: {}", e.toString(), e);
        }
    }

//    public void liftRobot()
//    {
//        if (isEnabled())
//        {
//            try
//            {
//                winchExtendSlashLetOutToLowerRobot();
//                hookRetract();
//            }
//            catch (Exception e)
//            {
//                logger.warn("An exception occurred in liftRobot. Cause Summary: {}", e.toString(), e);
//            }
//        }
//    }

    @Override
    public void initDefaultCommand()
    {
        if (isEnabled())
        {
//            setDefaultCommand(new UpdateWinchSpeedsViaDashboardCommand());
        }
    }


    private static final String WINCH_RAISE_SPEED_KEY = "WSS2 Winch Raise Speed";

    private static final String WINCH_LOWER_SPEED_KEY = "WSS2 Winch Lower Speed";

    private static final String HOOK_RAISE_SPEED_KEY = "WSS2 Hook Raise Speed";

    private static final String HOOK_LOWER_SPEED_KEY = "WSS2 Hook Lower Speed";

    static
    {
        SmartDashboard.putNumber(WINCH_RAISE_SPEED_KEY, 0.1); // 1.0
        SmartDashboard.putNumber(WINCH_LOWER_SPEED_KEY, 0.1); //.8
        SmartDashboard.putNumber(HOOK_RAISE_SPEED_KEY, 0.1);   // 0.16
        SmartDashboard.putNumber(HOOK_LOWER_SPEED_KEY, 0.1); // 0.10
    }


    public static void updateSpeedsFromSmartdashboard()
    {
        if (LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE)
        {
            winchRaiseSpeed = SmartDashboard.getNumber(WINCH_RAISE_SPEED_KEY, 0.1);
            winchLowerSpeed = SmartDashboard.getNumber(WINCH_LOWER_SPEED_KEY, 0.1);
            hookRaiseSpeed = SmartDashboard.getNumber(HOOK_RAISE_SPEED_KEY, 0.1);
            hookLowerSpeed = SmartDashboard.getNumber(HOOK_LOWER_SPEED_KEY, 0.1);
        }
    }


    public boolean isInManualMode()
    {
        return inManualMode;
    }


    public void setInManualMode(boolean inManualMode)
    {
        this.inManualMode = inManualMode;
    }


    public static boolean isEnabled()
    {
        return Enablings.isWinchSubsystemEnabled;
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final WinchSubsystem singleton = new WinchSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new WinchSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     WinchSubsystem subsystem = new WinchSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     WinchSubsystem subsystem = WinchSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static WinchSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/
}
