package frc.team3838.controls.joysticks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.controls.ControlAction;



@SuppressWarnings("UnusedDeclaration")
public enum ButtonAction implements ControlAction<Button>
{
    /** Cancel the command when the button is pressed. */
    CANCEL_WHEN_PRESSED
        {
            @Override
            public void assign(Button trigger, Command command)
            {

                logAssignment(trigger, command, this);
                trigger.cancelWhenPressed(command);
            }
        },
    /** Toggles the command whenever the button is pressed (on then off then on). */
    TOGGLE_WHEN_PRESSED
        {
            @Override
            public void assign(Button trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.toggleWhenPressed(command);
            }
        },
    /** Starts the given command whenever the button is newly pressed. */
    WHEN_PRESSED
        {
            @Override
            public void assign(Button trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.whenPressed(command);
            }
        },
    /** Starts the command when the button is released. */
    WHEN_RELEASED
        {
            @Override
            public void assign(Button trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.whenReleased(command);
            }
        },
    /** Constantly starts the given command while the button is held. Command.start() will be called
     * repeatedly while the button is held, and will be canceled when the button is released. */
     WHILE_HELD
        {
            @Override
            public void assign(Button trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.whileHeld(command);
            }
        }

;
    private static final Logger logger = LoggerFactory.getLogger(ButtonAction.class);


    private static void logAssignment(Button trigger, Command command, ButtonAction action)
    {
        if (logger.isDebugEnabled())
        {
            final String triggerName = (trigger instanceof NamedJoystickButton) ? ((NamedJoystickButton) trigger).getButtonName() : "unnamed";
            logger.debug("Assigning {} {} the command {}::{}", triggerName, action.name(), command.getName(), command.getClass().getName());
        }
    }

}
