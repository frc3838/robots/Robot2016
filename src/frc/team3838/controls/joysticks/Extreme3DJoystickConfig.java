package frc.team3838.controls.joysticks;

import java.util.SortedMap;
import java.util.TreeMap;

import com.google.common.collect.ImmutableSortedMap;


/**
 * An enumeration of the joysticks on the <em>Logitech Extreme 3D Joystick</em>.
 */
@SuppressWarnings("UnusedDeclaration")
public enum Extreme3DJoystickConfig implements IJoystickConfig
{
    TRIGGER_1
        {
            @Override
            public int getButtonNumber()
            {
                return 1;
            }
        },
    SIDE_THUMB_2
        {
            @Override
            public int getButtonNumber()
            {
                return 2;
            }
        },
    STICK_LEFT_LOWER_3
        {
            @Override
            public int getButtonNumber()
            {
                return 3;
            }
        },
    STICK_RIGHT_LOWER_4
        {
            @Override
            public int getButtonNumber()
            {
                return 4;
            }
        },
    STICK_LEFT_UPPER_5
        {
            @Override
            public int getButtonNumber()
            {
                return 53;
            }
        },
    STICK_RIGHT_UPPER_6
        {
            @Override
            public int getButtonNumber()
            {
                return 6;
            }
        },
    BASE_UPPER_RIGHT_7
        {
            @Override
            public int getButtonNumber()
            {
                return 7;
            }
        },
    BASE_UPPER_LEFT_8
        {
            @Override
            public int getButtonNumber()
            {
                return 8;
            }
        },
    BASE_MIDDLE_RIGHT_9
        {
            @Override
            public int getButtonNumber()
            {
                return 9;
            }
        },
    BASE_MIDDLE_LEFT_10
        {
            @Override
            public int getButtonNumber()
            {
                return 10;
            }
        },
    BASE_LOWER_RIGHT_11
        {
            @Override
            public int getButtonNumber()
            {
                return 11;
            }
        },
    BASE_LOWER_LEFT_12
        {
            @Override
            public int getButtonNumber()
            {
                return 12;
            }
        };

    protected static ImmutableSortedMap<Integer, Extreme3DJoystickConfig> map;


    static
    {
        final Extreme3DJoystickConfig[] values = values();
        SortedMap<Integer, Extreme3DJoystickConfig> map = new TreeMap<>();
        for (Extreme3DJoystickConfig value : values)
        {
            map.put(value.getButtonNumber(), value);
        }
        Extreme3DJoystickConfig.map = ImmutableSortedMap.copyOfSorted(map);
    }


    public static Extreme3DJoystickConfig lookupByButtonNumber(int buttonNumber)
    {
        return map.get(buttonNumber);
    }


    @Override
    public String getJoystickTypeName()
    {
        return "Logitech Extreme 3D Joystick";
    }
}
