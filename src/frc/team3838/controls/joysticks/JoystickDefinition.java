package frc.team3838.controls.joysticks;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Trigger;
import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.controls.ControlAction;



@SuppressWarnings("UnusedDeclaration")
public class JoystickDefinition<N extends Enum<N> & IJoystickConfig>
{
    private static final Logger logger = LoggerFactory.getLogger(JoystickDefinition.class);

    protected final ImmutableMap<String, NamedJoystickButton> buttonNamesMap;
    protected final ImmutableMap<N, NamedJoystickButton> buttonNameEnumMap;
    protected final ImmutableList<String> buttonNamesList;

    protected final ImmutableList<NamedJoystickButton> buttons;

    protected final Class<N> joystickType;
    protected final String joystickTypeName;
    protected final Joystick joystick;
    protected final String joystickName;
    protected final int joystickPort;


    protected JoystickDefinition(Class<N> joystickType, String joystickName, int joystickPort)
    {

        if (joystickType == null)
        {
            throw new IllegalArgumentException("'joystickType' cannot be null");
        }

        if (StringUtils.isBlank(joystickName))
        {
            throw new IllegalArgumentException("'joystickName' cannot be null, empty, or blank. Value received (in quotes): \"" + joystickName + '"');
        }

        try
        {
            joystick = new Joystick(joystickPort);
            this.joystickName = joystickName;
            this.joystickPort = joystickPort;
            this.joystickType = joystickType;

            final Builder<String> namesListBuilder = ImmutableList.builder();
            final Builder<NamedJoystickButton> buttonsListBuilder = ImmutableList.builder();
            final ImmutableMap.Builder<String, NamedJoystickButton> buttonNamesMapBuilder = ImmutableMap.builder();
            final ImmutableMap.Builder<N, NamedJoystickButton> buttonNamesEnumMapBuilder = ImmutableMap.builder();


            @SuppressWarnings("unchecked")
            N[] values = (N[]) joystickType.getDeclaredMethod("values").invoke(null);
            this.joystickTypeName = values[0].getJoystickTypeName();
            int buttonNumber = 1;
            for (N buttonNameEnum : values)
            {
                final String buttonName = buttonNameEnum.name();
                @SuppressWarnings("ObjectAllocationInLoop")
                NamedJoystickButton button = new NamedJoystickButton(joystick, joystickName, joystickPort, buttonName, buttonNumber++);
                buttonsListBuilder.add(button);
                namesListBuilder.add(buttonName);
                buttonNamesMapBuilder.put(buttonName, button);
                buttonNamesEnumMapBuilder.put(buttonNameEnum, button);
            }


            buttonNameEnumMap = buttonNamesEnumMapBuilder.build();
            buttonNamesMap = buttonNamesMapBuilder.build();
            buttonNamesList = namesListBuilder.build();
            buttons = buttonsListBuilder.build();

        }
        catch (Exception e)
        {
            String msg = String.format("Could not create %s Joystick definition due to an exception. Cause Details: %s", getClass(), e.toString());
            logger.error(msg, e);
            throw new RuntimeException(msg, e);
        }

    }


    public <T extends Trigger> void assignButton(T button, ControlAction<T> action, Command command) throws IllegalArgumentException
    {
        assignImpl(button, action, command);
    }


    public <T extends Trigger> void assignButton(int buttonNumber, ControlAction<T> action, Command command) throws IllegalArgumentException
    {
        @SuppressWarnings("unchecked")
        final T button = (T) getButton(buttonNumber);
        assignImpl(button, action, command);
    }

    public <T extends Trigger> void assignButton(String buttonName, ControlAction<T> action, Command command) throws IllegalArgumentException
    {
        @SuppressWarnings("unchecked")
        final T button = (T) getButton(buttonName);
        assignImpl(button, action, command);
    }


    public <T extends Trigger> void assignButton(N buttonName, ControlAction<T> action, Command command) throws IllegalArgumentException
    {
        @SuppressWarnings("unchecked")
        final T button = (T) getButton(buttonName);
        assignImpl(button, action, command);
    }


    protected  <T extends Trigger> void assignImpl(T button, ControlAction<T> action, Command command) {action.assign(button, command);}


    /**
     * Returns a joystick button by its number).
     *
     * <pre>
     *        ATTACK 3 JOYSTICK BUTTON LAYOUT
     * ***************************************
     * *             (1 Trigger)             *
     * *          .................          *
     * *          :               :          *
     * * +---+    :     +---+     :    +---+ *
     * * | 6 |    . +-+ | 3 | +-+ :    + 11| *
     * * +---+    : |4| +---+ |5| :    +---+ *
     * *          : +-+ +---+ +-+ :          *
     * * +---+    :     | 2 |     :    +---+ *
     * * | 7 |    :     +---+     :    | 10| *
     * * +---+    :...............:    +---+ *
     * * Left          Stick           Right *
     * * Group      +---+  +---+       Group *
     * *            | 8 |  | 9 |             *
     * *            +---+  +---+             *
     * *            Bottom Group             *
     * ***************************************
     * </pre>
     *
     * @param buttonNumber the button number to get.
     *                     *
     * @return the joystick button for the button number
     *
     * @throws IllegalArgumentException if the @code buttonNumber} is out of the allowable range
     */
    public NamedJoystickButton getButton(int buttonNumber) throws IllegalArgumentException
    {
        if ((buttonNumber < 1) || (buttonNumber > 11))
        {
            String message = buttonNumber + " is not a valid Joystick button number. Must be between 1-11";
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
        return buttons.get(buttonNumber - 1);
    }

    public NamedJoystickButton getButton(String  buttonName) throws IllegalArgumentException
    {
        final N buttonNameEnum = Enum.valueOf(joystickType, buttonName);
        return getButton(buttonNameEnum);
    }


    private NamedJoystickButton getButton(N buttonNameEnum) {return buttonNameEnumMap.get(buttonNameEnum);}


    public ImmutableList<NamedJoystickButton> getButtons()
    {
        return buttons;
    }


    public Joystick getJoystick()
    {
        return joystick;
    }


    /**
     * Returns the name of the Joystick definition such as 'Driver', 'Shooter', 'Ops', etc.
     * @return the name of the Joystick definition such as 'Driver', 'Shooter', 'Ops', etc.
     */
    public String getJoystickName()
    {
        return joystickName;
    }


    /**
     * Returns the port number of the joystick.
     * @return the port number of the joystick.
     */
    public int getJoystickPort()
    {
        return joystickPort;
    }


    public Class<N> getJoystickType()
    {
        return joystickType;
    }


    /**
     * Returns the simple name of the joystick type, such as 'Extreme 3D Joystick', or 'Attack 3 Joystick'.
     * @return the simple name of the joystick type, such as 'Extreme 3D Joystick', or 'Attack 3 Joystick'.
     */
    public String getJoystickTypeName()
    {
        return joystickTypeName;
    }


    @Override
    @SuppressWarnings({"MethodWithMultipleReturnPoints", "RedundantIfStatement", "OverlyComplexMethod", "QuestionableName", "UnclearBinaryExpression", "ControlFlowStatementWithoutBraces"})
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JoystickDefinition<?> that = (JoystickDefinition<?>) o;

        if (joystickPort != that.joystickPort) return false;
        if (!joystickName.equals(that.joystickName)) return false;
        if (!joystickType.equals(that.joystickType)) return false;

        return true;
    }


    @Override
    public int hashCode()
    {
        int result = joystickType.hashCode();
        result = 31 * result + joystickName.hashCode();
        result = 31 * result + joystickPort;
        return result;
    }
}
