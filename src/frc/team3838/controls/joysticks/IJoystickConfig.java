package frc.team3838.controls.joysticks;

public interface IJoystickConfig
{
    int getButtonNumber();

    String getJoystickTypeName();
}
