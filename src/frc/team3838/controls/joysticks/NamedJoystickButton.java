package frc.team3838.controls.joysticks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.JoystickButton;



public class NamedJoystickButton extends JoystickButton
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(NamedJoystickButton.class);


    private final GenericHID joystick;
    private final String joystickName;
    private final String buttonName;
    private final int joystickPort;
    private final int buttonNumber;


    /**
     * Create a joystick button for triggering commands
     *
     * @param joystick     The GenericHID object that has the button (e.g. Joystick, KinectStick, etc)
     * @param joystickName The name of the joystick (used in identifying information such as toString, equals, and hashCode)
     * @param joystickPort The port of the joystick (used in identifying information such as toString, equals, and hashCode)
     * @param buttonName   The button name
     */
    NamedJoystickButton(GenericHID joystick, String joystickName, int joystickPort, String buttonName, int buttonNumber)
    {
        super(joystick, buttonNumber);
        this.buttonName = buttonName;
        this.joystick = joystick;
        this.joystickPort = joystickPort;
        this.joystickName = joystickName;
        this.buttonNumber = buttonNumber;
    }


    public String getButtonName()
    {
        return buttonName;
    }


    /** @noinspection RedundantIfStatement, QuestionableName, MethodWithMultipleReturnPoints, OverlyComplexMethod */
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if ((o == null) || (getClass() != o.getClass())) return false;

        NamedJoystickButton that = (NamedJoystickButton) o;

        if (joystickPort != that.joystickPort) return false;
        if (buttonNumber != that.buttonNumber) return false;
        if (!buttonName.equals(that.buttonName)) return false;
        if (!joystick.equals(that.joystick)) return false;
        if (!joystickName.equals(that.joystickName)) return false;

        return true;
    }


    @SuppressWarnings("UnclearExpression")
    public int hashCode()
    {
        int result = buttonName.hashCode();
        result = 31 * result + joystick.hashCode();
        result = 31 * result + joystickName.hashCode();
        result = 31 * result + joystickPort;
        result = 31 * result + buttonNumber;
        return result;
    }


    public String toString()
    {
        return buttonName + " [#" + buttonNumber +"] on " + joystickName + " (port #" + joystickPort + ')';
    }


}

