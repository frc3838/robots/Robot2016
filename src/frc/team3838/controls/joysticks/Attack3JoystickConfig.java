package frc.team3838.controls.joysticks;

import java.util.SortedMap;
import java.util.TreeMap;

import com.google.common.collect.ImmutableSortedMap;



/**
 * An enumeration of the joysticks on the <em>Logitech Attack 3 Joystick</em>.
 *
 * <pre>
 *        JOYSTICK BUTTON LAYOUT
 * ***************************************
 * *             (1 Trigger)             *
 * *          .................          *
 * *          :               :          *
 * * +---+    :     +---+     :    +---+ *
 * * | 6 |    . +-+ | 3 | +-+ :    + 11| *
 * * +---+    : |4| +---+ |5| :    +---+ *
 * *          : +-+ +---+ +-+ :          *
 * * +---+    :     | 2 |     :    +---+ *
 * * | 7 |    :     +---+     :    | 10| *
 * * +---+    :...............:    +---+ *
 * * Left          Stick           Right *
 * * Group      +---+  +---+       Group *
 * *            | 8 |  | 9 |             *
 * *            +---+  +---+             *
 * *            Bottom Group             *
 * ***************************************
 * </pre>
 */
@SuppressWarnings("UnusedDeclaration")
public enum Attack3JoystickConfig implements IJoystickConfig
{
    //List in button order
    TRIGGER_1
        {
            @Override
            public int getButtonNumber()
            {
                return 1;
            }
        },
    HIGH_HAT_BOTTOM_2
        {
            @Override
            public int getButtonNumber()
            {
                return 2;
            }
        },
    HIGH_HAT_TOP_3
        {
            @Override
            public int getButtonNumber()
            {
                return 3;
            }
        },
    HIGH_HAT_LEFT_4
        {
            @Override
            public int getButtonNumber()
            {
                return 4;
            }
        },
    HIGH_HAT_RIGHT_5
        {
            @Override
            public int getButtonNumber()
            {
                return 5;
            }
        },
    LEFT_GROUP_UPPER_6
        {
            @Override
            public int getButtonNumber()
            {
                return 6;
            }
        },
    LEFT_GROUP_LOWER_7
        {
            @Override
            public int getButtonNumber()
            {
                return 7;
            }
        },
    BOTTOM_GROUP_LEFT_8
        {
            @Override
            public int getButtonNumber()
            {
                return 8;
            }
        },
    BOTTOM_GROUP_RIGHT_9
        {
            @Override
            public int getButtonNumber()
            {
                return 9;
            }
        },
    RIGHT_GROUP_LOWER_10
        {
            @Override
            public int getButtonNumber()
            {
                return 10;
            }
        },
    RIGHT_GROUP_UPPER_11
        {
            @Override
            public int getButtonNumber()
            {
                return 11;
            }
        };

    protected static ImmutableSortedMap<Integer, Attack3JoystickConfig> map;

    static
    {
        final Attack3JoystickConfig[] values = values();
        SortedMap<Integer, Attack3JoystickConfig> map = new TreeMap<>();
        for (Attack3JoystickConfig value : values)
        {
            map.put(value.getButtonNumber(), value);
        }
        Attack3JoystickConfig.map = ImmutableSortedMap.copyOfSorted(map);
    }


    public static Attack3JoystickConfig lookupByButtonNumber(int buttonNumber)
    {
        return map.get(buttonNumber);
    }


    @Override
    public String getJoystickTypeName()
    {
        return "Logitech Attack 3 Joystick";
    }

}
