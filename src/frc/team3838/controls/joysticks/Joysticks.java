package frc.team3838.controls.joysticks;

import java.lang.reflect.Constructor;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.commands.DeactivatedCommandStandInCommand;
import frc.team3838.commands.NoOpCommand;
import frc.team3838.commands.ballomatic.BallomaticArmManuallyDownUnshiftedCommand;
import frc.team3838.commands.ballomatic.BallomaticArmManuallyUpUnshiftedCommand;
import frc.team3838.commands.ballomatic.BallomaticArmStopCommand;
import frc.team3838.commands.ballomatic.BallomaticFeedMotorDischargeCommand;
import frc.team3838.commands.ballomatic.BallomaticFeedMotorIntakeCommand;
import frc.team3838.commands.ballomatic.BallomaticFeedMotorStopCommand;
import frc.team3838.commands.ballomatic.BallomaticMoveToMidPosition;
import frc.team3838.commands.ballomatic.BallomaticMoveToPickPosition;
import frc.team3838.commands.ballomatic.TurnOffWinchManualModeCommand;
import frc.team3838.commands.ballomatic.TurnOnWinchManualModeCommand;
import frc.team3838.commands.winch.ExtendWinchHookCommand;
import frc.team3838.commands.winch.RetractWinchHookCommand;
import frc.team3838.commands.winch.WinchLowerRobotComboCommand;
import frc.team3838.commands.winch.WinchRaiseRobotComboCommand;
import frc.team3838.commands.winch.WinchStopAllCommand;
import frc.team3838.controls.joysticks.F310GamePad.DPadButtons;



public class Joysticks
{
    private static final Logger logger = LoggerFactory.getLogger(Joysticks.class);


    private Joysticks() { }


    //public static final JoystickDefinition<Extreme3DJoystickConfig> DRIVER = new JoystickDefinition<>(Extreme3DJoystickConfig.class, "DRIVER", 0);
//    public static final JoystickDefinition<Attack3JoystickConfig> DRIVER_LEFT = new JoystickDefinition<>(Attack3JoystickConfig.class, "DRIVER_LEFT", 0);
//    public static final JoystickDefinition<Attack3JoystickConfig> DRIVER_RIGHT = new JoystickDefinition<>(Attack3JoystickConfig.class, "DRIVER_RIGHT", 1);
    public static final JoystickDefinition<F310GamePad> GAMEPAD= new JoystickDefinition<>(F310GamePad.class, "GAMEPAD", 2);
    public static final JoystickDefinition<Attack3JoystickConfig> LIFT_ARMS = new JoystickDefinition<>(Attack3JoystickConfig.class, "LIFT_ARMS", 3);


    // ==== BUTTON ASSIGNMENTS ===/



    // Static initializer that Initializes the joysticks and the joystick button assignments.
    static {initButtons();}

    /** Initializes the joysticks and the joystick button assignments. * */
    public static void init() { /* no op method (at this time) that simply ensures the static initializer block is run at a desired time. */ }


    /**
     * Assigns command to a joystick button by its number (1-11).
     *
     * <pre>
     *        JOYSTICK BUTTON LAYOUT
     * ***************************************
     * *             (1 Trigger)             *
     * *          .................          *
     * *          :               :          *
     * * +---+    :     +---+     :    +---+ *
     * * | 6 |    . +-+ | 3 | +-+ :    + 11| *
     * * +---+    : |4| +---+ |5| :    +---+ *
     * *          : +-+ +---+ +-+ :          *
     * * +---+    :     | 2 |     :    +---+ *
     * * | 7 |    :     +---+     :    | 10| *
     * * +---+    :...............:    +---+ *
     * * Left          Stick           Right *
     * * Group      +---+  +---+       Group *
     * *            | 8 |  | 9 |             *
     * *            +---+  +---+             *
     * *            Bottom Group             *
     * ***************************************
     * </pre>
     */
    private static void initButtons()
    {

        // @formatter:off

        /* 1*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.TRIGGER_1,           ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 2*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.SIDE_THUMB_2,        ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 3*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_LEFT_LOWER_3,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 4*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_LOWER_4, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 5*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_LEFT_UPPER_5,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 6*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_UPPER_6, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 7*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_UPPER_RIGHT_7,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 8*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_UPPER_LEFT_8,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /* 9*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_RIGHT_9, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /*10*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_LEFT_10, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /*11*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_RIGHT_11, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /*12*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_LEFT_12,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        /*11*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_RIGHT_11, ButtonAction.WHEN_PRESSED, cmd(WinchResetMinMaxReadingsCommand.class));
        /*12*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_LEFT_12,  ButtonAction.WHEN_PRESSED, cmd(WinchEncoderResetCommand.class));



//        /* 1*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /* 2*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_PRESSED,  cmd(BallomaticFeedMotorIntakeCommand.class));
//        /* 2*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_RELEASED, cmd(BallomaticFeedMotorStopCommand.class));
//        /* 3*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_PRESSED,  cmd(BallomaticFeedMotorDischargeCommand.class));
//        /* 3*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_RELEASED, cmd(BallomaticFeedMotorStopCommand.class));
//        /* 4*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.HIGH_HAT_LEFT_4,        ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /* 5*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.HIGH_HAT_RIGHT_5,       ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /* 6*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.LEFT_GROUP_UPPER_6,     ButtonAction.WHEN_PRESSED,  new DrivePidRotateCommand(45.0));
//        /* 7*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.LEFT_GROUP_LOWER_7,     ButtonAction.WHEN_PRESSED,  new DrivePidRotateCommand(90.0));
//        /* 8*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED,  new DrivePidRotateCommand(180.0));
//        /* 8*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED,  new DrivePidRotateCommand(-45.0));
//        /* 9*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_RIGHT_9,   ButtonAction.WHEN_PRESSED,  new DrivePidRotateCommand(-180.0));
//        /*10*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.RIGHT_GROUP_LOWER_10,   ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /*11*/ Joysticks.DRIVER_LEFT.assignButton(Attack3JoystickConfig.RIGHT_GROUP_UPPER_11,   ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));


//        /* 1*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_PRESSED,  cmd(BallomaticTurnOnManualMode.class));
//        /* 1*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_RELEASED, cmd(BallomaticTurnOffManualMode.class));
//        /* 2*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_PRESSED,  cmd(BallomaticArmManuallyDownCommand.class));
//        /* 2*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_RELEASED, cmd(BallomaticArmStopCommand.class));
//        /* 3*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_PRESSED,  cmd(BallomaticArmManuallyUpCommand.class));
//        /* 3*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_RELEASED, cmd(BallomaticArmStopCommand.class));
//        /* 4*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.HIGH_HAT_LEFT_4,        ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /* 5*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.HIGH_HAT_RIGHT_5,       ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /* 6*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.LEFT_GROUP_UPPER_6,     ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /* 7*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.LEFT_GROUP_LOWER_7,     ButtonAction.WHEN_PRESSED,  cmd(BallomaticMoveToMidPosition.class));
//        /* 8*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /* 9*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_RIGHT_9,   ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
//        /*10*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.RIGHT_GROUP_LOWER_10,   ButtonAction.WHEN_PRESSED,  cmd(BallomaticMoveToPickPosition.class));
//        /*11*/ Joysticks.DRIVER_RIGHT.assignButton(Attack3JoystickConfig.RIGHT_GROUP_UPPER_11,   ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));



//        Joysticks.GAMEPAD.assignButton(F310GamePad.GREEN_A_DOWN,                     ButtonAction.WHEN_PRESSED,  cmd(WinchLowerRobotComboCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.GREEN_A_DOWN,                     ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.RED_B_RIGHT,                       ButtonAction.WHEN_PRESSED,  cmd(ExtendWinchHookCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.RED_B_RIGHT,                       ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.BLUE_X_LEFT,                      ButtonAction.WHEN_PRESSED,  cmd(RetractWinchHookCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.BLUE_X_LEFT,                      ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.YELLOW_Y_UP,                    ButtonAction.WHEN_PRESSED,  cmd(WinchRaiseRobotComboCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.YELLOW_Y_UP,                    ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));

        Joysticks.GAMEPAD.assignButton(F310GamePad.GREEN_A_DOWN,                   ButtonAction.WHEN_PRESSED,   cmd(RetractWinchHookCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.GREEN_A_DOWN,                   ButtonAction.WHEN_RELEASED,  cmd(WinchStopAllCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.RED_B_RIGHT,                    ButtonAction.WHEN_PRESSED,   cmd(BallomaticArmManuallyDownUnshiftedCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.RED_B_RIGHT,                    ButtonAction.WHEN_RELEASED,  cmd(BallomaticArmStopCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.BLUE_X_LEFT,                    ButtonAction.WHEN_PRESSED,   cmd(BallomaticArmManuallyUpUnshiftedCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.BLUE_X_LEFT,                    ButtonAction.WHEN_RELEASED,  cmd(BallomaticArmStopCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.YELLOW_Y_UP,                    ButtonAction.WHEN_PRESSED,   cmd(ExtendWinchHookCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.YELLOW_Y_UP,                    ButtonAction.WHEN_RELEASED,  cmd(WinchStopAllCommand.class));




//        Joysticks.GAMEPAD.assignButton(F310GamePad.BACK_LEFT_ARROW,             ButtonAction.WHEN_PRESSED,  cmd(WinchRetractSlashPullInToRaiseRobotCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.BACK_LEFT_ARROW,             ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.START_RIGHT_ARROW,           ButtonAction.WHEN_PRESSED,  cmd(WinchExtendSlashLetOutToLowerRobotCommand.class));
//        Joysticks.GAMEPAD.assignButton(F310GamePad.START_RIGHT_ARROW,           ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));

        Joysticks.GAMEPAD.assignButton(F310GamePad.BACK_LEFT_ARROW,             ButtonAction.WHEN_PRESSED,  cmd(TurnOnWinchManualModeCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.BACK_LEFT_ARROW,             ButtonAction.WHEN_RELEASED, cmd(TurnOffWinchManualModeCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.START_RIGHT_ARROW,           ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.START_RIGHT_ARROW,           ButtonAction.WHEN_RELEASED, cmd(NoOpCommand.class));


        Joysticks.GAMEPAD.assignButton(F310GamePad.LEFT_JOY_BTN,                ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.RIGHT_JOY_BTN,               ButtonAction.WHEN_PRESSED,  cmd(NoOpCommand.class));

        Joysticks.GAMEPAD.assignButton(F310GamePad.getLeftThrottleAsButton(Joysticks.GAMEPAD), ButtonAction.WHEN_PRESSED, cmd(BallomaticMoveToPickPosition.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.LB,                          ButtonAction.WHEN_PRESSED,  cmd(BallomaticMoveToMidPosition.class));

        Joysticks.GAMEPAD.assignButton(F310GamePad.RB,                          ButtonAction.WHEN_PRESSED,  cmd(BallomaticFeedMotorDischargeCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.RB,                          ButtonAction.WHEN_RELEASED, cmd(BallomaticFeedMotorStopCommand.class));

        Joysticks.GAMEPAD.assignButton(F310GamePad.getRightThrottleAsButton(Joysticks.GAMEPAD), ButtonAction.WHEN_PRESSED, cmd(BallomaticFeedMotorIntakeCommand.class));
        Joysticks.GAMEPAD.assignButton(F310GamePad.getRightThrottleAsButton(Joysticks.GAMEPAD), ButtonAction.WHEN_RELEASED, cmd(BallomaticFeedMotorStopCommand.class));



        Joysticks.GAMEPAD.assignButton(DPadButtons.N.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_PRESSED, cmd(WinchRaiseRobotComboCommand.class));
        Joysticks.GAMEPAD.assignButton(DPadButtons.N.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));

        Joysticks.GAMEPAD.assignButton(DPadButtons.S.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_PRESSED, cmd(WinchLowerRobotComboCommand.class));
        Joysticks.GAMEPAD.assignButton(DPadButtons.S.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_RELEASED, cmd(WinchStopAllCommand.class));

        Joysticks.GAMEPAD.assignButton(DPadButtons.E.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        Joysticks.GAMEPAD.assignButton(DPadButtons.E.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_RELEASED, cmd(NoOpCommand.class));

        Joysticks.GAMEPAD.assignButton(DPadButtons.W.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));
        Joysticks.GAMEPAD.assignButton(DPadButtons.W.getButton(Joysticks.GAMEPAD), ButtonAction.WHEN_RELEASED, cmd(NoOpCommand.class));


        /*
            Ball Arm Manual down                        B-Red-Right
            Ball Arm Manual Up                          X-Blue-Left

            Hook Extend                                 Y-Yellow-Up
            Hook Retract                                A-Green-Down

            Raise Robot (lift winch & hook retract)     DPad-N-Up
            Lower Robot (lower winch & hook extract)    DPad-S-Down

            Winch Extend/slacken                        Shifted[left-Back]-DPad-N-Up
            Winch Retract/pull-in                       Shifted[left-Back]-DPad-S-Down

         */







        /* 1*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 2*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 3*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 4*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_LEFT_4,        ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 5*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_RIGHT_5,       ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 6*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.LEFT_GROUP_UPPER_6,     ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 7*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.LEFT_GROUP_LOWER_7,     ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 8*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 9*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_RIGHT_9,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /*10*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.RIGHT_GROUP_LOWER_10,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /*11*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.RIGHT_GROUP_UPPER_11,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/



        /* 1*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.TRIGGER_1,           ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 2*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.SIDE_THUMB_2,        ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 3*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_LEFT_LOWER_3,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 4*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_LOWER_4, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 5*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_LEFT_UPPER_5,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 6*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_UPPER_6, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 7*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_UPPER_RIGHT_7,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 8*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_UPPER_LEFT_8,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 9*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_RIGHT_9, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /*10*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_LEFT_10, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /*11*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_LOWER_RIGHT_11, ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /*11*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_LOWER_LEFT_12,  ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/

        /* 1*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 2*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 3*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 4*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_LEFT_4,        ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 5*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_RIGHT_5,       ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 6*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.LEFT_GROUP_UPPER_6,     ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 7*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.LEFT_GROUP_LOWER_7,     ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 8*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /* 9*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_RIGHT_9,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /*10*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.RIGHT_GROUP_LOWER_10,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/
        /*11*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.RIGHT_GROUP_UPPER_11,   ButtonAction.WHEN_PRESSED, cmd(NoOpCommand.class));*/



        // @formatter:on
    }



    private static Command cmd(@Nullable Class<? extends Command> commandClass, Object... parameters)
    {
        if (commandClass == null)
        {
            commandClass = NoOpCommand.class;
        }
        return getOrCreateCommand(commandClass, parameters);
    }


    private static Command getOrCreateCommand(@Nonnull Class<? extends Command> commandClass, Object... parameters)
    {

        logger.debug("Joysticks.getOrCreateCommand() method called for {}", commandClass.getName());

        try
        {

            logger.debug("Joysticks.getOrCreateCommand() creating instance of '{}", commandClass.getName());
            final Command command;
            if (NoOpCommand.class.isAssignableFrom(commandClass))
            {
                command = new NoOpCommand();
            }
            else
            {
                logger.debug("Attempting to reflectively create an instance of {}", commandClass.getName());

                if ((parameters == null) || (parameters.length == 0))
                {
                    command = commandClass.newInstance();
                }
                else
                {
                    Class<?>[] parameterTypes = new Class[parameters.length];
                    for (int i = 0; i < parameters.length; i++)
                    {
                        parameterTypes[i] = parameters[i].getClass();
                    }
                    final Constructor<? extends Command> constructor = commandClass.getConstructor(parameterTypes);
                    command =  constructor.newInstance(parameters);
                }
            }

            if (command == null)
            {
                throw new IllegalStateException("Command of type " + commandClass.getSimpleName() +  "  was not properly created");
            }


            return command;
        }
        catch (Exception e)
        {
            //noinspection UnnecessaryToStringCall
            logger.warn("Could not create an instance of the {}. Cause details: {}", commandClass.getSimpleName(), e.toString(), e);
            return new DeactivatedCommandStandInCommand(commandClass);
        }
    }

}
