package frc.team3838.controls.joysticks;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;



public class AxisToButton extends Button
{
    private static final Logger logger = LoggerFactory.getLogger(AxisToButton.class);

    @Nonnull
    private final Joystick joystick;

    private final int rawAxisNumber;


    private final double triggeringThreshold;


    public AxisToButton(@Nonnull Joystick joystick, int axisNumber)
    {
        this(joystick, axisNumber, 0.15);
    }

    public AxisToButton(@Nonnull Joystick joystick, int axisNumber, double triggeringThreshold)
    {
        this.joystick = joystick;
        this.rawAxisNumber = axisNumber;
        this.triggeringThreshold = Math.abs(triggeringThreshold);
    }


    @Override
    public boolean get()
    {
        return Math.abs(joystick.getRawAxis(rawAxisNumber)) >= triggeringThreshold;
    }
}
