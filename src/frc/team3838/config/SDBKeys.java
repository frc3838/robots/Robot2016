package frc.team3838.config;

/**
 * Smart Dashboard Keys.
 */
public final class SDBKeys
{
    public static final String AVG_DISTANCE = " Average Distance ";
    public static final String RIGHT_SIDE_DISTANCE = "Right Side Distance ";
    public static final String LEFT_SIDE_DISTANCE = "Left Side Distance ";



    private SDBKeys() { }
}
