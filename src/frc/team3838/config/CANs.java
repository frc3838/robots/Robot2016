package frc.team3838.config;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSortedMap;



/** Configuration 'constants' class for CAN Device ID assignments. */
@SuppressWarnings("PointlessArithmeticExpression")
public final class CANs
{

    /*
    CAN IDs are assigned in the roboRIO UI -- http://172.22.11.2 -- **ALWAYS** assign as ID as they default to Zero
    So keep Zero "Free". See Section 2.2. "Common ID Talons" of the "Talon SRX Software Reference Manual.pdf"
    in C:\Users\Developer\Downloads\FRC\Hardware Components\Talon SRX or download from
    http://www.ctr-electronics.com/talon-srx.html#product_tabs_technical_resources
    From that Section:

        TIP: Since the default device ID of an “out of the box” Talon SRX is device ID ‘0’, when you setup your
             robot for the first time, start assigning device IDs at ’1’. That way you can, at any time, add
             another default Talon to your bus and easily identify it.
    */


    private static final Logger logger = LoggerFactory.getLogger(CANs.class);

    //Keep IDs in numerical order please

    public static final int BALL_ARM_MOTOR = 1;


    @SuppressWarnings("unused")
    public static final int DEFAULT_AND_THEREFORE_RESERVED_ID = 0;

    public static final ImmutableSortedMap<Integer, String> mappings;



    static
    {
        logger.info("Mapping CAN assignments and checking for duplicates");
        Map<Integer, String>  tempMap = new HashMap<>();
        try
        {
            final Class<CANs> clazz = CANs.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()) && !"MXP_ADD_FACTOR".equals(field.getName()))
                {
                    final int id = field.getInt(null);
                    if (id != -1)
                    {
                        if (tempMap.containsKey(id))
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : CAN ID # %s is duplicated! It is assigned to both '%s' and '%s'", id, tempMap.get(id), field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                        else
                        {
                            tempMap.put(id, field.getName());
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //noinspection UnnecessaryToStringCall
            logger.warn("Could not validate the CANs fields due to an Exception. Cause Details: {}", e.toString(), e);
            tempMap = new HashMap<>();
        }
        mappings = ImmutableSortedMap.copyOf(tempMap);
    }


    private CANs() { }


    public static void init()
    {
        // a no op method to have the class initialize
    }
}
