package frc.team3838.config;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSortedMap;



/** Configuration 'constants' class for Digital I/O (DIO) port assignments. */
@SuppressWarnings("PointlessArithmeticExpression")
public class DIOs
{
    /** Factor added to port # on the MXP (myRIO Expansion Port) 'More Board'. Thus for
     *  DIO port 2 on the MXP More board is DIO 12 (2 + ADD_FACTOR) to the roboRIO.
     *  See <a href='www.revrobotics.com/product/more-board/>www.revrobotics.com/product/more-board/</a>*/
    public static final int MXP_ADD_FACTOR = 10;






//    public static final int WINCH_ENCODER_CH_A = MXP_ADD_FACTOR + 8;
//    public static final int WINCH_ENCODER_CH_B = MXP_ADD_FACTOR + 9;

    private static final Logger logger = LoggerFactory.getLogger(DIOs.class);

    public static final int ULTRASONIC_PING_CHANNEL_DO = -1;
    public static final int ULTRASONIC_ECHO_CHANNEL_DI = -1;

    //Encoders: Channel A is Blue   Channel B is Yellow
    public static final int LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A = 0;
    public static final int LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B = 1;
    public static final int RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A = 2;
    public static final int RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B = 3;
    public static final int LEFT_ARM_ENCODER_CH_A = 6;
    public static final int LEFT_ARM_ENCODER_CH_B = 7;
    public static final int RIGHT_ARM_ENCODER_CH_A = 8;
    public static final int RIGHT_ARM_ENCODER_CH_B = 9;

    @SuppressWarnings("unused")
    public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_14 = 14;
    @SuppressWarnings("unused")
    public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_15 = 15;
    @SuppressWarnings("unused")
    public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_16 = 16;
    @SuppressWarnings("unused")
    public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_17 = 17;


    public static final ImmutableSortedMap<Integer, String> mappings;





    static
    {
        logger.info("Mapping DIO assignments and checking for duplicates");
        Map<Integer, String> tempMap = new HashMap<>();
        try
        {
            final Class<DIOs> clazz = DIOs.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()) && !"MXP_ADD_FACTOR".equals(field.getName()))
                {
                    final int port = field.getInt(null);
                    if (port != -1)
                    {
                        if (tempMap.containsKey(port))
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : DIO port # %s is duplicated! It is assigned to both '%s' and '%s'", port, tempMap.get(port), field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                        else
                        {
                            tempMap.put(port, field.getName());
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //noinspection UnnecessaryToStringCall
            logger.warn("Could not validate the DIO fields due to an Exception. Cause Details: {}", e.toString(), e);
            tempMap = new HashMap<>();
        }
        mappings = ImmutableSortedMap.copyOf(tempMap);
    }

    private DIOs() { }

    public static void init()
    {
        // a no op method to have the DIOs class initialize
    }

}
