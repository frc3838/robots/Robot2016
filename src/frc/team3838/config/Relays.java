package frc.team3838.config;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSortedMap;



/**
 * Relay channel assignments.
 */
public final class Relays
{
    public static final ImmutableSortedMap<Integer, String> mappings;

    private static final Logger logger = LoggerFactory.getLogger(Relays.class);
    public static final int LEFT_ARM_SPIKE = 0;
    public static final int RIGHT_ARM_SPIKE = 1;


    static
    {
        logger.info("Mapping Relays assignments and checking for duplicates");

        Map<Integer, String> tempMap = new HashMap<>();
        try
        {
            final Class<Relays> clazz = Relays.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()))
                {
                    final int channel = field.getInt(null);
                    if (channel != -1)
                    {
                        if (tempMap.containsKey(channel))
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : Relays channel # %s is duplicated! It is assigned to both '%s' and '%s'", channel, tempMap.get(channel), field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                        else
                        {
                            tempMap.put(channel, field.getName());
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //noinspection UnnecessaryToStringCall
            logger.warn("Could not validate the Relays fields due to an Exception. Cause Details: {}", e.toString(), e);
            tempMap = new HashMap<>();
        }
        mappings = ImmutableSortedMap.copyOf(tempMap);
    }


    private Relays() { }


    public static void init()
    {
        // a no op method to have the Relays class initialize
    }
}
