package frc.team3838.config;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSortedMap;



/**
 * Configuration 'constants' class to assign PWM channels. The class is named with a 's' (PWMs) to
 * prevent confusion with the WPI Lib's PWM class.
 */
public final class PWMs
{

    /**
     * Factor added to channel # on the MXP (myRIO Expansion Port) 'More Board'. Thus for
     * PWM channel 2 on the MXP More board is PCM 12 (2 + MXP_ADD_FACTOR) to the roboRIO.
     * See <a href='www.revrobotics.com/product/more-board/>www.revrobotics.com/product/more-board/</a>
     */
    @SuppressWarnings("unused")
    public static final int MXP_ADD_FACTOR = 10;


    public static final int LEFT_DRIVE_TRAIN_MOTOR = 0;
    public static final int RIGHT_DRIVE_TRAIN_MOTOR = 1;
    public static final int WINCH_LIFT_MOTOR = 6;
    public static final int HOOK_EXTENSION_MOTOR = 7;

    public static final int BALL_INTAKE_MOTOR = 8;



    public static final ImmutableSortedMap<Integer, String> mappings;

    private static final Logger logger = LoggerFactory.getLogger(PWMs.class);


    static
    {
        logger.info("Mapping PWM Channel assignments and checking for duplicates");
        Map<Integer, String> tempMap = new HashMap<>();
        try
        {
            final Class<PWMs> clazz = PWMs.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()))
                {
                    final int channel = field.getInt(null);
                    if (channel != -1)
                    {
                        if (tempMap.containsKey(channel) && !"MXP_ADD_FACTOR".equals(field.getName()))
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : PWM Channel # %s is duplicated! It is assigned to both '%s' and '%s'", channel, tempMap.get(channel), field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                        else
                        {
                            tempMap.put(channel, field.getName());
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //noinspection UnnecessaryToStringCall
            logger.warn("Could not validate the PWM fields due to an Exception. Cause Details: {}", e.toString(), e);
            tempMap = new HashMap<>();
        }
        mappings = ImmutableSortedMap.copyOf(tempMap);
    }


    private PWMs() { }


    public static void init()
    {
        // a no op method to have the PWMs Channel class initialize
    }

}
