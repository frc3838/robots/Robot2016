package frc.team3838;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import frc.team3838.commands.autonomous.AutonomousSelector;
import frc.team3838.config.AIOs;
import frc.team3838.config.CANs;
import frc.team3838.config.DIOs;
import frc.team3838.config.Enablings;
import frc.team3838.config.PCM;
import frc.team3838.config.PWMs;
import frc.team3838.controls.joysticks.Joysticks;
import frc.team3838.subsystems.DioTestingSubsystem;
import frc.team3838.subsystems.DriveTrainSubsystem;
import frc.team3838.subsystems.GyroAccelSubsystem;
import frc.team3838.subsystems.LiftArmsSubsystem;
import frc.team3838.subsystems.SonarSubsystem;
import frc.team3838.utils.logging.LogbackProgrammaticConfiguration;



/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
// *IMPORTANT: If you rename this class or change its package, the corresponding
//             change needs to be made in the ${projectRoot}/build.properties
//             file
public class Robot3838 extends IterativeRobot
{
    private static final long COMMAND_SCHEDULER_RUN_INTERVAL_DURATION = 5;
    private static final TimeUnit COMMAND_SCHEDULER_RUN_INTERVAL_TIME_UNIT = TimeUnit.MILLISECONDS;


    //We have to init the logging framework programmatically before we create our logger
    private static Logger logger;

    private boolean bootCompleteMsgHasBeenLogged = false;
    @Nullable
    private ScheduledExecutorService scheduledExecutorService;


    @Nullable
    private Command autonomousCommand;


    // Robot-wide initialization code should go here. ONLY CALLED ONCE.
    // Warning: the Driver Station "Robot Code" light and FMS "Robot Ready"
    //          indicators will be off until RobotInit() exits. Code in RobotInit() that
    //          waits for enable will cause the robot to never indicate that the code is
    //          ready, causing the robot to be bypassed in a match.
    @Override
    @SuppressWarnings("RefusedBequest")
    public void robotInit()
    {
        try
        {
            bootCompleteMsgHasBeenLogged = false;

            try
            {
                LogbackProgrammaticConfiguration.configure();
                logger = LoggerFactory.getLogger(Robot3838.class);
                logger.info("Logging Initialization completed. Initializing Robot...");
            }
            catch (Exception e)
            {
                //noinspection UseOfSystemOutOrSystemErr
                System.err.println(">>>CRITICAL ERROR<<< COULD NOT INITIALIZE THE LOGGING SYSTEM. CAUSE SUMMARY: " + e.toString());
                //noinspection CallToPrintStackTrace
                e.printStackTrace();
            }

            try
            {
                AutonomousSelector.setupChooser();
                GyroAccelSubsystem.getInstance().calibrateGyro();
                GyroAccelSubsystem.getInstance().resetGyro();
            }
            catch (Exception e)
            {
                logger.error("An exception occurred when calibrating and resetting the {}. Cause Summary: {}", GyroAccelSubsystem.class.getSimpleName(), e.toString(), e);
            }



            initPortAndChannelAssignments();
            // Because commands use, via requires() method, subsystems, subsystems need
            // to be initialized before commands, and thus Joysticks since he Joystick
            // class creates command instances.
            initSubsystems();
            Joysticks.init();


//        chooser = new SendableChooser();
//        chooser.addDefault("Default Auto", defaultAuto);
//        chooser.addObject("My Auto", customAuto);
//        SmartDashboard.putData("Auto choices", chooser);
        }
        catch (Exception e)
        {
            logger.error(">>>FATAL<<< A fatal Exception occurred in the robotInit() method. Cause Summary: {}", e.toString(), e);
        }
    }


    // Initialization code for autonomous mode should go here.
    @SuppressWarnings("RefusedBequest")
    public void autonomousInit()
    {
        logger.trace("autonomousInit() method called");
        startScheduler(false);
        if (DriveTrainSubsystem.isEnabled())
        {
            DriveTrainSubsystem.getInstance().disableMotorSafetyForAutonomousMode();
            autonomousCommand = AutonomousSelector.selectCommand();
            if (autonomousCommand != null)
            {
                logger.info("Autonomous mode selector returned: {}", autonomousCommand.getName());
                autonomousCommand.start();
            }
        }
    }


    /** This function is called periodically during autonomous mode, */
    @SuppressWarnings("RefusedBequest")
    public void autonomousPeriodic()
    {
        logger.trace("autonomousPeriodic() called");
//        switch (autoSelected)
//        {
//            case customAuto:
//                //Put custom auto code here
//                break;
//            case defaultAuto:
//            default:
//                //Put default auto code here
//                break;
//        }
    }



    // Called each time the robot enters teleop mode. Use to initialize any code that needs such each time teleop is entered.
    @Override
    @SuppressWarnings("RefusedBequest")
    public void teleopInit()
    {
        // This makes sure that the autonomous stops running when
        // teleop starts running. If you want the autonomous to
        // continue until interrupted by another command, remove
        // this line or comment it out.
        if (autonomousCommand != null) {autonomousCommand.cancel();}

        if (DriveTrainSubsystem.isEnabled())
        {
            DriveTrainSubsystem.getInstance().stop();
            DriveTrainSubsystem.getInstance().enableMotorSafetyPostAutonomousMode();
            DriveTrainSubsystem.getInstance().stop();
        }
        startScheduler(true);
    }


    // Called periodically during operator control (i.e. teleop mode),
    @Override
    @SuppressWarnings("RefusedBequest")
    public void teleopPeriodic()
    {
        //logger.trace("teleopPeriodic() called");
    }


    // Initialization code for test mode
    @Override
    @SuppressWarnings("RefusedBequest")
    public void testInit()
    {
        logger.trace("testInit() called");
        LiveWindow.run();

        if (autonomousCommand != null) {autonomousCommand.cancel();}

        if (DriveTrainSubsystem.isEnabled())
        {
            DriveTrainSubsystem.getInstance().stop();
            DriveTrainSubsystem.getInstance().enableMotorSafetyPostAutonomousMode();
            DriveTrainSubsystem.getInstance().stop();
        }
        startScheduler(true);
    }


    /** This function is called periodically during test mode. */
    @Override
    @SuppressWarnings("RefusedBequest")
    public void testPeriodic()
    {
        logger.trace("testPeriodic() called");
        //Not sure if we need the run() call in the periodic method or no
        LiveWindow.run();
    }


    @Override
    @SuppressWarnings("RefusedBequest")
    public void disabledInit()
    {
        logger.info("Robot entering disabled mode");
        stopScheduler();
        if (autonomousCommand != null) {autonomousCommand.cancel();}

        //This call really isn't needed... but we prefer to err on the side of safety
        if (DriveTrainSubsystem.isEnabled())
        {
            DriveTrainSubsystem.getInstance().stop();
        }

        if (!bootCompleteMsgHasBeenLogged)
        {
            bootCompleteMsgHasBeenLogged = true;
            logger.info("");
            logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            logger.info("~ ROBOT BOOT UP COMPLETED ~");
            logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            logger.info("");
        }
    }


    /**
     * Periodic code for disabled mode should go here.
     *
     * Users should override this method for code which will be called
     * periodically at a regular rate while the robot is in disabled mode.
     */
    @Override
    @SuppressWarnings("RefusedBequest")
    public void disabledPeriodic()
    {
        //logger.trace("disabledPeriodic() called");
    }


    private static void initPortAndChannelAssignments()
    {
        logger.debug("{}.initPortAndChannelAssignments() called", Robot3838.class.getSimpleName());
        AIOs.init();
        CANs.init();
        DIOs.init();
        PWMs.init();
        PCM.init();
        logger.debug("{}.initPortAndChannelAssignments() completed", Robot3838.class.getSimpleName());
    }

    private void startScheduler(boolean isInTeleopMode)
    {
        logger.debug("startScheduler() called");
        if (scheduledExecutorService == null)
        {

            scheduledExecutorService = Executors.newScheduledThreadPool(3);
            scheduledExecutorService.scheduleAtFixedRate((Runnable) () ->
            {
                // Schedule the Robot command scheduler
                try
                {
                    logger.trace("Calling Scheduler.getInstance().run()");
                    Scheduler.getInstance().run();
                }
                catch (Exception ex)
                {
                    logger.error("CRITICAL ERROR: Scheduler.getInstance().run() threw an exception. Cause Summary: {}", ex.toString(), ex);
                }

            }, 0, COMMAND_SCHEDULER_RUN_INTERVAL_DURATION, COMMAND_SCHEDULER_RUN_INTERVAL_TIME_UNIT);

            if (isInTeleopMode)
            {
                try
                {
                    //Just an extra safety insurance
                    DriveTrainSubsystem.getInstance().stop();
                }
                catch (Exception ignore) {}

                //Schedule the Drive command
                if (DriveTrainSubsystem.isEnabled())
                {
                    scheduledExecutorService.scheduleAtFixedRate((Runnable) () ->
                    {
                        try
                        {
                            DriveTrainSubsystem.getInstance().drive();
                        }
                        catch (Exception ex)
                        {
                            logger.error("CRITICAL ERROR: DriveTrainSubsystem.getInstance().drive() threw an exception. Cause Summary: {}", ex.toString(), ex);
                        }

                    }, 0, 5, TimeUnit.MILLISECONDS);
                }

                if (LiftArmsSubsystem.isEnabled())
                {
                    scheduledExecutorService.scheduleAtFixedRate((Runnable) () ->
                    {
                        try
                        {
                            LiftArmsSubsystem.getInstance().periodicExecute(Joysticks.LIFT_ARMS.getJoystick());
                        }
                        catch (Exception ex)
                        {
                            logger.error("LiftArmsSubsystem.periodicExecute() threw an exception. Cause Summary: {}", ex.toString(), ex);
                        }

                    }, 0, LiftArmsSubsystem.PERIODIC_EXECUTE_INTERVAL_DURATION, LiftArmsSubsystem.PERIODIC_EXECUTE_INTERVAL_TIME_UNIT);
                }
            }
        }
        else
        {
            logger.debug("scheduledExecutorService already running.");
        }
    }

    private void stopScheduler()
    {
        logger.debug("stopScheduler() called");
        try
        {
            if (scheduledExecutorService != null)
            {
                scheduledExecutorService.shutdownNow();
            }
        }
        catch (Exception e)
        {
            logger.warn("An Exception was thrown when doing hard shutdown on the scheduledExecutorService. Cause Summary: {}", e.toString(), e);
        }
        finally
        {
            scheduledExecutorService = null;
        }
    }

    private static void initSubsystems()
    {

        final ImmutableSet<Class<? extends Subsystem>> obsoleteSubsystems = ImmutableSet.of(
                                                                                  GyroAccelSubsystem.class,
                                                                                  SonarSubsystem.class,
                                                                                  DioTestingSubsystem.class);

        logger.debug("{}.initSubsystems() called", Robot3838.class.getSimpleName());
        try
        {
            final String pkgName = "frc.team3838.subsystems";
            final ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(pkgName))
                .setScanners(new SubTypesScanner())
                .filterInputsBy(new FilterBuilder().includePackage(pkgName));
            final Reflections reflections = new Reflections(configurationBuilder);
            final Set<Class<? extends Subsystem>> subsystems = reflections.getSubTypesOf(Subsystem.class);
            logger.debug("Found Subsystems: {}", subsystems);

            for (Class<? extends Subsystem> subsystem : subsystems)
            {
                try
                {
//                    if (!GyroAccelSubsystem.class.equals(subsystem))
                    if (!obsoleteSubsystems.contains(subsystem))
                    {
                        //We just call the getInstanceMethod so the Subsystem is initialized (if it is enabled in the Enablings class)
                        logger.debug("Reflectively initializing '{}'", subsystem.getSimpleName());
                        final Method getInstanceMethod = subsystem.getDeclaredMethod("getInstance");
                        getInstanceMethod.invoke(null);
                    }

                }
                catch (NoSuchMethodException e)
                {
                    logger.error("The '{}' class does not have a public static 'getInstance()' method and therefore cannot be automatically initialized via reflection. "
                                + "All Subsystems should be singletons and implement a public static 'getInstance()' method.", subsystem.getName());
                }
                catch (IllegalAccessException e)
                {
                    logger.error("The 'getInstance()' method for the '{}' class does not have public access and therefore cannot be automatically initialized via reflection. "
                                + "All Subsystems should be singletons and implement a public static 'getInstance()' method.", subsystem.getName());
                }
                catch (NullPointerException e)
                {
                    logger.error("The 'getInstance()' method for the '{}' class is not static and therefore cannot be automatically initialized via reflection. "
                                + "All Subsystems should be singletons and implement a public static 'getInstance()' method.", subsystem.getName());
                }
                catch (InvocationTargetException | ExceptionInInitializerError e)
                {
                    logger.error("The '{}' threw an Exception during initialization and therefore could not be initialized.  "
                                + "The public static 'getInstance()' method in all subsystems (or the subsystems constructors) should catch and log initialization "
                                 + "exceptions, and then mark the subsystem as disabled in the {} class. Exception Details: {}",
                                 subsystem.getName(), Enablings.class.getName(), e.toString(), e);
                }
                catch (Exception e)
                {
                    logger.error("The '{}' could not be initialized due to an Exception. Exception Details: {}",
                                 subsystem.getName(), e.toString(), e);
                }
            }
            logger.debug("{}.initSubsystems() completed", Robot3838.class.getSimpleName());
        }
        catch (Exception e)
        {
            logger.error("An error occurred when reflectively initializing subsystems. Cause summary: {}", e.toString(), e);
        }

    }

}
