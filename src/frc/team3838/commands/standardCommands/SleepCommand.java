package frc.team3838.commands.standardCommands;


import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;


public class SleepCommand extends Command
{
    private static final Logger logger = LoggerFactory.getLogger(SleepCommand.class);

    private long startTime;
    private long delay;
    private boolean finished = false;
    private boolean isFirstCall = true;


    public static SleepCommand createSeconds(long seconds)
    {
        return new SleepCommand(TimeUnit.SECONDS.toMillis(seconds));
    }

    public static SleepCommand createSeconds(long seconds, PartialSeconds partialSeconds)
    {
        return new SleepCommand(TimeUnit.SECONDS.toMillis(seconds) + partialSeconds.toMills());
    }


    public static SleepCommand create(long duration, TimeUnit timeUnit)
    {
        return new SleepCommand(timeUnit.toMillis(duration));
    }


    public static SleepCommand create(long duration1, TimeUnit timeUnit1, long duration2, TimeUnit timeUnit2)
    {
        long totalDurationInMs = timeUnit1.toMillis(duration1) + timeUnit2.toMillis(duration2);
        return new SleepCommand(totalDurationInMs);
    }



    public static SleepCommand create(long duration1, TimeUnit timeUnit1, long duration2, TimeUnit timeUnit2, long duration3, TimeUnit timeUnit3)
    {
        long totalDurationInMs = timeUnit1.toMillis(duration1) + timeUnit2.toMillis(duration2) + timeUnit3.toMillis(duration3);
        return new SleepCommand(totalDurationInMs);
    }


    private SleepCommand(long milliseconds)
    {
        delay = milliseconds;
        logger.debug("SLEEP command created for {}ms", delay);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    private void startTimer()
    {
        finished = false;
        startTime = System.currentTimeMillis();
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (isFirstCall)
        {
            startTimer();
            isFirstCall = false;
            logger.debug("Executing Sleep Command for {} ms", delay);
        }
        long diff = System.currentTimeMillis() - startTime;
        finished = diff >= delay;
        if (finished)
        {
            logger.debug("SLEEP Seconds Command Stopping after {}ms", delay);
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        if (finished)
        {
            logger.debug("Sleep Command of {} ms has completes", delay);
        }
        return finished;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
        //reset for the next run
        isFirstCall = true;
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }

    public static enum PartialSeconds
    {
        NoPartial
            {
                @Override
                long toMills()
                {
                    return 0;
                }
            },
        EighthSecond
            {
                @Override
                long toMills()
                {
                    return 125;
                }
            },
        QuarterSecond
            {
                @Override
                long toMills()
                {
                    return 250;
                }
            },
        ThreeEightsSecond
            {
                @Override
                long toMills()
                {
                    return 375;
                }
            },
        HalfSecond
            {
                @Override
                long toMills()
                {
                    return 500;
                }
            },
        FiveEightsSecond
            {
                @Override
                long toMills()
                {
                    return 625;
                }
            },
        ThreeQuartersSecond
            {
                @Override
                long toMills()
                {
                    return 750;
                }
            },
        SevenEightsSecond
            {
                @Override
                long toMills()
                {
                    return 875;
                }
            };

        abstract long toMills();
    }
}
