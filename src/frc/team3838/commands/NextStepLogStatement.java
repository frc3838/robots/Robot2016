package frc.team3838.commands;

import edu.wpi.first.wpilibj.command.Command;



/**
 * Simple command that logs a statement that the next step in an autonomous sequence is starting.
 * Used when "recording" via logging driver actions when determining actions needed to mimic
 * driver actions.
 */
public class NextStepLogStatement extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NextStepLogStatement.class);


    public NextStepLogStatement()
    {

    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        // no op
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.info("================================================================================");
        logger.info("================================================================================");
        logger.info("  >>> NEXT STEP...");
        logger.info("================================================================================");
        logger.info("================================================================================");
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {

        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
