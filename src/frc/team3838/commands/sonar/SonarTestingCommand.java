package frc.team3838.commands.sonar;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.subsystems.SonarSubsystem;
import frc.team3838.utils.MathUtils;



public class SonarTestingCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SonarTestingCommand.class);


    // *** All Subsystem classes MUST be Singletons and obtained via a static getInstance() method. ***
    // Instances should also be static to ensure they are initialized prior to the command

    protected static final SonarSubsystem sonarSubsystem = SonarSubsystem.getInstance();


    public SonarTestingCommand()
    {
        logger.trace("{} constructor called", getClass().getSimpleName());
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields be singletons
        // and obtained via a static getInstance() method
        // eg. requires(driveTrainSubsystem);
        //     requires(shooterSubsystem);
        requires(sonarSubsystem);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (areAllSubsystemsAreEnabled())
        {
            logger.debug("{}.initialize() called", getClass().getSimpleName());
            // No op
        }
        else
        {
            logger.warn("Not all required subsystems for {} are enabled, thus the command can not be and will not be initialized or executed.", getClass().getSimpleName());
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.trace("{}.execute() called", getClass().getSimpleName());
        if (areAllSubsystemsAreEnabled())
        {
            logger.trace("Executing Command '{}' ", getClass().getSimpleName());
            try
            {

                final double distanceForDigitalSonarInMM = sonarSubsystem.readDistanceForDigitalInMM();
                final double distanceForDigitalSonarInInches = sonarSubsystem.readDistanceForDigitalSonarInInches();

                SmartDashboard.putString("Sonar Distance mm", MathUtils.formatNumber(distanceForDigitalSonarInMM, 3) );
                SmartDashboard.putString("Sonar Distance in", MathUtils.formatNumber(distanceForDigitalSonarInInches, 3) );

//                final double distanceForAnalogSonarInInches = sonarSubsystem.readDistanceForAnalogSonar();
//                logger.trace("Analog Sonar Distance: {}\"", MathUtils.formatNumber(distanceForAnalogSonarInInches, 3));
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        logger.trace("{}.isFinished() called", getClass().getSimpleName());
        return false;
    }


    protected static boolean areAllSubsystemsAreEnabled()
    {
        return SonarSubsystem.isEnabled();
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.trace("{}.end() called", getClass().getSimpleName());
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.debug("{}.interrupted() called", getClass().getSimpleName());
    }
}
