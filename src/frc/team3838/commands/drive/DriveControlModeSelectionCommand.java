package frc.team3838.commands.drive;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import frc.team3838.subsystems.DriveTrainSubsystem;
import frc.team3838.subsystems.DriveTrainSubsystem.ControlMode;



public class DriveControlModeSelectionCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveControlModeSelectionCommand.class);

    public static SendableChooser driveModeChooser;

    private static boolean initWarnMessageHasBeenLogged = false;

    // *** All Subsystem classes MUST be Singletons and obtained via a static getInstance() method. ***
    // Instances should also be static to ensure they are initialized prior to the command

    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();


    public DriveControlModeSelectionCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields be singletons
        // and obtained via a static getInstance() method
        // eg. requires(driveTrainSubsystem);
        //     requires(shooterSubsystem);
        requires(driveTrainSubsystem);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (areAllSubsystemsAreEnabled())
        {
            logger.debug("{}.initialize() called", getClass().getSimpleName());

            driveTrainSubsystem.sdbPutDistances();

            driveModeChooser = new SendableChooser();
            final ControlMode[] controlModes = ControlMode.values();
            for (ControlMode controlMode : controlModes)
            {
                if (controlMode == controlModes[3])
                {
                    driveModeChooser.addDefault(controlMode.getDescription(), controlMode);
                }
                else
                {
                    driveModeChooser.addObject(controlMode.getDescription(), controlMode);;
                }
            }

//            SmartDashboard.putData("Drive Control Mode Selector", driveModeChooser);
        }
        else
        {
            if (!initWarnMessageHasBeenLogged)
            {
                logger.warn("Not all required subsystems for {} are enabled, thus the command can not be and will not be initialized or executed.", getClass().getSimpleName());
                initWarnMessageHasBeenLogged = true;
            }
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.trace("{}.execute() called", getClass().getSimpleName());
        if (areAllSubsystemsAreEnabled() && (driveModeChooser != null))
        {
            logger.trace("Executing Command '{}' ", getClass().getSimpleName());
            try
            {
                //DriveTrainSubsystem.ControlMode controlMode = (ControlMode) driveModeChooser.getSelected();
                DriveTrainSubsystem.ControlMode controlMode = ControlMode.GAMEPAD_TANK;
                driveTrainSubsystem.setControlMode(controlMode);
                logger.trace("Setting Drive Train Control Mode to {}", controlMode.getDescription());
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        logger.trace("{}.isFinished() called", getClass().getSimpleName());
        //TODO: Set this properly
        return true;
    }


    protected static boolean areAllSubsystemsAreEnabled()
    {
        return (DriveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.trace("{}.end() called", getClass().getSimpleName());
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.debug("{}.interrupted() called", getClass().getSimpleName());
    }
}
