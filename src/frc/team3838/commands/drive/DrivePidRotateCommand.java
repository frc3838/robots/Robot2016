package frc.team3838.commands.drive;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.subsystems.DriveTrainSubsystem;
import frc.team3838.subsystems.NavxSubsystem;



public class DrivePidRotateCommand extends Command implements PIDOutput
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DrivePidRotateCommand.class);

    private static boolean initWarnMessageHasBeenLogged = false;

    // *** All Subsystem classes MUST be Singletons and obtained via a static getInstance() method. ***
    // Instances should also be static to ensure they are initialized prior to the command

    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
    protected static final NavxSubsystem navxSubsystem = NavxSubsystem.getInstance();

    /* The following PID Controller coefficients will need to be tuned */
    /* to match the dynamics of your drive system.  Note that the      */
    /* SmartDashboard in Test mode has support for helping you tune    */
    /* controllers by displaying a form where you can enter new P, I,  */
    /* and D constants and test the mechanism.                         */

    static final double kP = 0.03;
    static final double kI = 0.00;
    static final double kD = 0.00;
    static final double kF = 0.00;
    static final double kToleranceDegrees = 0.5f;

    PIDController turnController;

    /**
     * The angle to rotate, relative to zero at NavxReset time. In other words,
     * this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     * rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     * and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     * start position or last Navx Reset), and you pass in 180° it will rotate 90°
     * (or possibly 270°) to the 180° point. Value must be in the range of
     * -180° to 180
     */
    private final double targetAngle;

    private double rotateToAngleRate;


    /**
     * @param targetAngle the angle to rotate, relative to zero at NavxReset time. In other words,
     *                    this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     *                    rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     *                    and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     *                    start position or last Navx Reset), and you pass in 180° it will rotate 90°
     *                    (or possibly 270°) to the 180° point. Value must be in the range of
     *                    -180° to 180
     */
    public DrivePidRotateCommand(int targetAngle)
    {
        this((double) targetAngle);
    }

    /**
     *
     * @param targetAngle the angle to rotate, relative to zero at NavxReset time. In other words,
     *                          this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     *                          rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     *                          and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     *                          start position or last Navx Reset), and you pass in 180° it will rotate 90°
     *                          (or possibly 270°) to the 180° point. Value must be in the range of
     *                          -180° to 180
     *
     */
    public DrivePidRotateCommand(double targetAngle)
    {
        super(DrivePidRotateCommand.class.getSimpleName() + '_' + targetAngle, 2);
        final double originalAngle = targetAngle;
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields be singletons
        // and obtained via a static getInstance() method
        // eg. requires(driveTrainSubsystem);
        //     requires(shooterSubsystem);
        logger.debug("{} constructor called with angle of {}", getName(), originalAngle);
        requires(driveTrainSubsystem);
        requires(navxSubsystem);
        if ((targetAngle > 180) || (targetAngle < -180))
        {
            throw new IllegalArgumentException("'targetAngle' must be between -180.0 and 180.0");
        }

        if (targetAngle == 180)
        {
            targetAngle = 179.9;
        }
        else if (targetAngle == -180)
        {
            targetAngle = -179.9;
        }

        this.targetAngle = targetAngle;
        logger.debug("{} created with angle of {} for desired angle of {}", getName(), this.targetAngle, originalAngle);
    }





    // Called just before this Command runs the first time
    @SuppressWarnings("Duplicates")
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (areAllSubsystemsAreEnabled() && (navxSubsystem.getNavx() != null))
        {
            logger.debug("{}.initialize() called", getName());

            turnController = new PIDController(kP, kI, kD, kF, navxSubsystem.getNavx(), this);
            turnController.setInputRange(-180.0f, 180.0f);
            turnController.setOutputRange(-1.0, 1.0);
            turnController.setAbsoluteTolerance(kToleranceDegrees);
            turnController.setContinuous(true);
            turnController.enable();
            driveTrainSubsystem.enableMotorSafetyPostAutonomousMode();
            driveTrainSubsystem.setExpiration(0.1);

        }
        else
        {
            if (!initWarnMessageHasBeenLogged)
            {
                logger.warn("Not all required subsystems for {} are enabled, thus the command can not be and will not be initialized or executed.", getName());
                initWarnMessageHasBeenLogged = true;
            }
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.trace("{}.execute() called", getName());
        if (areAllSubsystemsAreEnabled())
        {
            logger.trace("Executing Command '{}' with targetAngle = {} and rotateToAngleRate = {}", getName(), targetAngle, rotateToAngleRate);
            try
            {
                if (isFinished())
                {
                    end();
                }
                else
                {
                    logger.trace("calling turnController.setSetpoint({})", targetAngle);
                    turnController.setSetpoint(targetAngle);
                    logger.trace("Calling driveViaArcadeDrive(0, {})", rotateToAngleRate);
                    driveTrainSubsystem.driveViaArcadeDrive(0, rotateToAngleRate, false, DriveTrainSubsystem.STALL_SPEED + 0.05);
                    Timer.delay(0.005); // wait for a motor update time
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        if (!areAllSubsystemsAreEnabled() || (turnController == null))
        {
            return true;
        }
        else
        {
            final boolean done = turnController.onTarget() || isTimedOut();
            if (done)
            {
                driveTrainSubsystem.stop();
            }
            return done;
        }
    }


    protected static boolean areAllSubsystemsAreEnabled()
    {
        return (DriveTrainSubsystem.isEnabled() && NavxSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.trace("{}.end() called", getName());
        driveTrainSubsystem.stop();
        driveTrainSubsystem.disableMotorSafetyForAutonomousMode();
        driveTrainSubsystem.setExpiration(10);
        turnController.disable();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.debug("{}.interrupted() called", getName());
    }


    @Override
    public void pidWrite(double output)
    {
        /* This method is invoked periodically by the PID Controller, */
        /* based upon navX MXP yaw angle input and PID Coefficients.    */
        this.rotateToAngleRate = output;
    }
}
