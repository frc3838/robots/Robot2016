package frc.team3838.commands.autonomous;

public class AutonomousDriveOverRoughObstacleCommand extends AutonomousAbstractDriveOverObstacleCommand
{
    public AutonomousDriveOverRoughObstacleCommand()
    {
        super(0.95);
    }
}
