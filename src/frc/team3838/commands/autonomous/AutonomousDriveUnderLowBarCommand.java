package frc.team3838.commands.autonomous;

public class AutonomousDriveUnderLowBarCommand extends AutonomousAbstractDriveOverObstacleCommand
{
    public AutonomousDriveUnderLowBarCommand()
    {
        super(0.35);
    }
}
