package frc.team3838.commands.autonomous;

import edu.wpi.first.wpilibj.command.Command;



public class LogCommandNameCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LogCommandNameCommand.class);

    private final Command commandToLog;

    public LogCommandNameCommand(Command commandToLog)
    {
        this.commandToLog = commandToLog;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.info("Running {}", commandToLog.getName());
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
