package frc.team3838.commands.autonomous;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.subsystems.DriveTrainSubsystem;
import frc.team3838.subsystems.NavxSubsystem;



public class AutonomousAbstractDriveOverObstacleCommand extends Command
{
    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
    protected static final NavxSubsystem navxSubsystem = NavxSubsystem.getInstance();
    protected static final  double PITCH_WOBBLE_TOLERANCE = 0.2;
    @SuppressWarnings("UnusedDeclaration")
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected double startingPitch;
    protected double currentPitch;
    protected boolean hasStartedUp = false;
    protected boolean hasCompletedUp = false;
    protected boolean hasStartedDown = false;
    protected boolean hasCompletedDown = false;
    protected long downCompletionTime;
    protected double speed;


    public AutonomousAbstractDriveOverObstacleCommand(double speed)
    {
        this.speed = speed;
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields be singletons
        // and obtained via a static getInstance() method
        // eg. requires(driveTrainSubsystem);
        //     requires(shooterSubsystem);
        requires(driveTrainSubsystem);
        requires(navxSubsystem);
    }


    protected static boolean areAllSubsystemsAreEnabled()
    {
        return (DriveTrainSubsystem.isEnabled() && NavxSubsystem.isEnabled() && (navxSubsystem != null));
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (areAllSubsystemsAreEnabled())
        {
            logger.debug("{}.initialize() called", getClass().getSimpleName());
            startingPitch = navxSubsystem.getStartingPitch();
            logger.debug("logger = '{}'", logger);
            hasStartedUp = false;
            hasCompletedUp = false;
            hasStartedDown = false;
            hasCompletedDown = false;
        }
        else
        {
            logger.warn("Not all required subsystems for {} are enabled, thus the command can not be and will not be initialized or executed.", getClass().getSimpleName());
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.trace("{}.execute() called", getClass().getSimpleName());
        if (areAllSubsystemsAreEnabled())
        {
            logger.trace("Executing Command '{}' ", getClass().getSimpleName());
            try
            {
                // The navxSubsystem.isEnabled checks is the navX component is null
                //noinspection ConstantConditions
                currentPitch = navxSubsystem.getNavx().getPitch();
                logger.debug("currentPitch = '{}'", currentPitch);

                //Going up ia a positive pitch, going down is a negative pitch
                if (!hasStartedUp)
                {
                    hasStartedUp = currentPitch > (startingPitch + PITCH_WOBBLE_TOLERANCE);
                    if (hasStartedUp)
                    {
                        logger.info("HAS STARTED UP");
                    }
                }

                if (hasStartedUp && !hasCompletedUp)
                {
                    hasCompletedUp = ((currentPitch <= (startingPitch + PITCH_WOBBLE_TOLERANCE)) || (currentPitch >= (startingPitch - PITCH_WOBBLE_TOLERANCE)));
                    if (hasCompletedUp)
                    {
                        logger.info("HAS COMPLETED UP");
                    }
                }

                if (hasCompletedUp && !hasStartedDown)
                {
                    hasStartedDown = currentPitch < (startingPitch - PITCH_WOBBLE_TOLERANCE);
                    if (hasStartedDown)
                    {
                        logger.info("HAS STARTED DOWN");
                    }
                }

                if (hasStartedDown && !hasCompletedDown)
                {
                    hasCompletedDown = ((currentPitch >= (startingPitch + PITCH_WOBBLE_TOLERANCE)) || (currentPitch <= (startingPitch - PITCH_WOBBLE_TOLERANCE)));
                    if (hasCompletedDown)
                    {

                        logger.info("HAS COMPLETED DOWN");
                        downCompletionTime = System.currentTimeMillis();
                    }

                }

                //TODO: Need to remove dashboard reading
                //Low bar, use 0.8
                //Main obs use 1.0
                driveTrainSubsystem.driveStraightForward(speed);



            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        logger.trace("{}.isFinished() called", getClass().getSimpleName());

        return hasCompletedDown /*&& ((System.currentTimeMillis() - downCompletionTime) >= 5)*/;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.trace("{}.end() called", getClass().getSimpleName());
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.debug("{}.interrupted() called", getClass().getSimpleName());
    }
}
