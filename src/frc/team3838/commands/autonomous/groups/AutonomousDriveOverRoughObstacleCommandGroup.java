package frc.team3838.commands.autonomous.groups;


import frc.team3838.commands.autonomous.AutonomousDisableMotorSafetyCommand;
import frc.team3838.commands.autonomous.AutonomousDriveOverRoughObstacleCommand;
import frc.team3838.commands.autonomous.AutonomousEnableMotorSafetyCommand;
import frc.team3838.commands.drive.DriveSetDistanceCommand;
import frc.team3838.commands.drive.StopDrivingCommand;



public class AutonomousDriveOverRoughObstacleCommandGroup extends AutonomousAbstractCommandGroup
{
    public AutonomousDriveOverRoughObstacleCommandGroup() { super(); }

    public AutonomousDriveOverRoughObstacleCommandGroup(String name) { super(name); }


    @Override
    protected void addCommands()
    {
        //Drive forward
        addSequential(new AutonomousDisableMotorSafetyCommand());
        addSequential(new AutonomousDriveOverRoughObstacleCommand());
        addSequential(DriveSetDistanceCommand.forwardFeet(3));
        addSequential(new StopDrivingCommand());
        addSequential(new AutonomousEnableMotorSafetyCommand());
    }
}