package frc.team3838.commands.autonomous.groups;


import frc.team3838.commands.autonomous.AutonomousDisableMotorSafetyCommand;
import frc.team3838.commands.autonomous.AutonomousDriveOverRoughObstacleCommand;
import frc.team3838.commands.autonomous.AutonomousEnableMotorSafetyCommand;
import frc.team3838.commands.ballomatic.BallomaticFeedMotorDischargeCommand;
import frc.team3838.commands.ballomatic.BallomaticFeedMotorStopCommand;
import frc.team3838.commands.ballomatic.BallomaticMoveToMidPosition;
import frc.team3838.commands.ballomatic.BallomaticMoveToPickPosition;
import frc.team3838.commands.drive.DrivePidRotateCommand;
import frc.team3838.commands.drive.DriveSetDistanceCommand;
import frc.team3838.commands.drive.StopDrivingCommand;
import frc.team3838.commands.standardCommands.SleepCommand;
import frc.team3838.commands.standardCommands.SleepCommand.PartialSeconds;



public class AutonomousDriveUnderLowBarAndGoForwardAndShootCommandGroup extends AutonomousAbstractCommandGroup
{
    public AutonomousDriveUnderLowBarAndGoForwardAndShootCommandGroup() { super(); }


    public AutonomousDriveUnderLowBarAndGoForwardAndShootCommandGroup(String name) { super(name); }


    @Override
    protected void addCommands()
    {
        addSequential(new AutonomousDisableMotorSafetyCommand());
        addSequential(new BallomaticMoveToPickPosition());
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));
        addSequential(new AutonomousDriveOverRoughObstacleCommand());
        addSequential(new StopDrivingCommand());
        addSequential(DriveSetDistanceCommand.forwardFeetAndInches(7, 8));
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));
        addSequential(new DrivePidRotateCommand(60.0));
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));
        addSequential(new BallomaticFeedMotorDischargeCommand());
        addSequential(SleepCommand.createSeconds(1));
        addSequential(new BallomaticFeedMotorStopCommand());
        addSequential(new BallomaticMoveToMidPosition());
        addSequential(new AutonomousEnableMotorSafetyCommand());
    }
}