package frc.team3838.commands.autonomous.groups;


import frc.team3838.commands.autonomous.AutonomousDisableMotorSafetyCommand;
import frc.team3838.commands.autonomous.AutonomousDriveUnderLowBarCommand;
import frc.team3838.commands.autonomous.AutonomousEnableMotorSafetyCommand;
import frc.team3838.commands.ballomatic.BallomaticMoveToMidPosition;
import frc.team3838.commands.ballomatic.BallomaticMoveToPickPosition;
import frc.team3838.commands.drive.DriveSetDistanceCommand;
import frc.team3838.commands.drive.StopDrivingCommand;
import frc.team3838.commands.standardCommands.SleepCommand;
import frc.team3838.commands.standardCommands.SleepCommand.PartialSeconds;



public class AutonomousDriveUnderLowBarCommandGroup extends AutonomousAbstractCommandGroup
{
    public AutonomousDriveUnderLowBarCommandGroup() { super(); }

    public AutonomousDriveUnderLowBarCommandGroup(String name) { super(name); }


    @Override
    protected void addCommands()
    {
        //Drive forward
        addSequential(new AutonomousDisableMotorSafetyCommand());
        addSequential(new BallomaticMoveToPickPosition());
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));
        addSequential(new AutonomousDriveUnderLowBarCommand());
        addSequential(DriveSetDistanceCommand.forwardFeetAndInches(2, 0));
        addSequential(new StopDrivingCommand());
        addSequential(new BallomaticMoveToMidPosition());
        addSequential(new AutonomousEnableMotorSafetyCommand());
    }
}