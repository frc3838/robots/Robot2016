package frc.team3838.commands.autonomous.groups;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team3838.commands.autonomous.AutonomousDisableMotorSafetyCommand;
import frc.team3838.commands.autonomous.LogCommandGroupFinishedCommand;
import frc.team3838.commands.autonomous.LogCommandGroupStartCommand;
import frc.team3838.subsystems.BallomaticSubsystem;
import frc.team3838.subsystems.DriveTrainSubsystem;
import frc.team3838.subsystems.LiftArmsSubsystem;



@SuppressWarnings("AbstractClassExtendsConcreteClass")
public abstract class AutonomousAbstractCommandGroup extends CommandGroup
{
    @SuppressWarnings("UnusedDeclaration")
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected boolean isFirstCall = true;
    protected final LogCommandGroupStartCommand logStartCommand;


    protected AutonomousAbstractCommandGroup()
    {
        super();
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        addCommands();
        postCommands();
    }


    protected AutonomousAbstractCommandGroup(String name)
    {
        super(name);
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        addCommands();
        postCommands();
    }


    protected AutonomousAbstractCommandGroup(List<Command> sequentialCommands)
    {
        super();
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        sequentialCommands.forEach(this::addSequential);
        addCommands();
        postCommands();
    }

    protected AutonomousAbstractCommandGroup(String name, List<Command> sequentialCommands)
    {
        super(name);
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        sequentialCommands.forEach(this::addSequential);
        addCommands();
        postCommands();
    }



    protected void addRequiredSubSystems()
    {
        requires(LiftArmsSubsystem.getInstance());
        requires(BallomaticSubsystem.getInstance());
        requires(DriveTrainSubsystem.getInstance());
    }


    protected void preCommands()
    {
        addSequential(logStartCommand);
        addSequential(new AutonomousDisableMotorSafetyCommand());
    }


    protected void postCommands()
    {
        addSequential(new LogCommandGroupFinishedCommand(this, logStartCommand));
        addSequential(new AutonomousDisableMotorSafetyCommand());
    }



    /**
     * Add commands in this method:
     * <pre>
     *      Run in Sequence
     *          e.g. addSequential(new Command1());
     *               addSequential(new Command2());
     *      these will run in order.
     *
     *      To run multiple commands at the same time, use addParallel()
     *          e.g. addParallel(new Command1());
     *               addSequential(new Command2());
     *      Command1 and Command2 will run in parallel.
     *
     *      A command group will require all of the subsystems that each member would require.
     *          e.g. if Command1 requires chassis, and Command2 requires arm,
     *          a CommandGroup containing them would require both the chassis and the arm.
     * </pre>
     */
    protected abstract void addCommands();


    @Override
    protected void execute()
    {
        if (isFirstCall)
        {
            logger.info("Executing {}", getClass().getSimpleName());
            isFirstCall = false;
        }
        super.execute();
    }


    @Override
    protected void end()
    {
        super.end();
        isFirstCall = true;
    }


    @Override
    public synchronized void start()
    {
        logger.info("{}.start() called", getClass().getSimpleName());
        super.start();
    }
}