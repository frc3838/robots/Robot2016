package frc.team3838.commands.autonomous;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.commands.autonomous.groups.AutonomousDriveOverRoughObstacleCommandGroup;
import frc.team3838.commands.autonomous.groups.AutonomousDriveUnderLowBarAndGoForwardAndShootCommandGroup;
import frc.team3838.commands.autonomous.groups.AutonomousDriveUnderLowBarAndGoForwardCommandGroup;
import frc.team3838.commands.autonomous.groups.AutonomousDriveUnderLowBarCommandGroup;



public class AutonomousSelector
{
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(AutonomousSelector.class);

    public static final SendableChooser autoModeChooser = new SendableChooser();


    private static final Command doNothingCommand = new AutonomousDoNothing();


    private static final Command underLowBarAndShoot = new AutonomousDriveUnderLowBarAndGoForwardAndShootCommandGroup();
    private static final Command underLowBarAndPosition = new AutonomousDriveUnderLowBarAndGoForwardCommandGroup();
    private static final Command driveUnderLowBar = new AutonomousDriveUnderLowBarCommandGroup();
    private static final Command driveRoughOverObstacle = new AutonomousDriveOverRoughObstacleCommandGroup();





    public static Command selectCommand()
    {
        return (Command) autoModeChooser.getSelected();
    }


    public static void setupChooser()
    {
        try
        {
            logger.info("Setting up autonomous mode chooser...");

            autoModeChooser.addObject("Drive Under Low Bar and shoot", underLowBarAndShoot);
            autoModeChooser.addObject("Drive Under Low Bar and *position/prep* for teleop", underLowBarAndPosition);
            autoModeChooser.addObject("Drive Under Low Bar", driveUnderLowBar);
            autoModeChooser.addObject("Dive Over Rough Obstacle", driveRoughOverObstacle);


            autoModeChooser.addDefault("DEFAULT: No Autonomous Mode / Do Nothing (Default)", doNothingCommand);
            SmartDashboard.putData("Autonomous Modes Selection...", autoModeChooser);
            logger.info("Autonomous mode chooser set up completed.");
        }
        catch (Throwable e)
        {
            final String msg = String.format("An error occurred when setting up Autonomous Chooser. Summary: %s", e.toString());
            DriverStation.reportError(msg, false);
            logger.error(msg, e);
        }
    }
}