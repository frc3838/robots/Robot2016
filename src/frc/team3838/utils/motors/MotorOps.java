package frc.team3838.utils.motors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.livewindow.LiveWindowSendable;
import edu.wpi.first.wpilibj.tables.ITable;
import frc.team3838.utils.MathUtils;



/** @noinspection UnusedDeclaration */ /* @noinspection UnusedDeclaration */
public class MotorOps implements PIDOutput,
                                 LiveWindowSendable

{
    private static final Logger logger = LoggerFactory.getLogger(MotorOps.class);

    // Motor settings
    //    setRaw() ranges from 0 to 255. 128 to 255 is forward speed, and 127 to 0 is reversed
    //    set() ranges from -1 to 1 and is provided for victor.

    private static final double STOP = 0;
    public static final double DEFAULT_MIN_SPEED = 0.01;

    private boolean inverse;
    private int inverseFactor;

    private double minNonStopSpeed = DEFAULT_MIN_SPEED;

    /** The speed <b>without</b> alteration for forward or reversed. */
    private double speed;

    private SpeedController controller;
    private LiveWindowSendable liveWindowSendable;


    /**
     * @param speedController the {@link SpeedController} (CANJaguar, Jaguar, Talon or Victor) to control
     * @param inverse         if the motor should be inverted via software
     */
    public MotorOps(SpeedController speedController, boolean inverse)
    {
        this(speedController, inverse, DEFAULT_MIN_SPEED);
    }


    /**
     * @param speedController the {@link SpeedController} (CANJaguar, Jaguar, Talon or Victor) to control
     * @param inverse         if the motor should be inverted via software
     * @param minNonStopSpeed the minimum speed (to prevent motor stalling or burn out)
     */
    public MotorOps(SpeedController speedController, boolean inverse, double minNonStopSpeed)
    {
        if (!(speedController instanceof LiveWindowSendable))
        {
            throw new IllegalArgumentException("Supplied SpeedController does not implement LiveWindowSendable interface... it needs to.");
        }
        this.controller = speedController;
        this.liveWindowSendable = (LiveWindowSendable) controller;
        this.inverse = inverse;
        inverseFactor = inverse ? -1 : 1;
        this.minNonStopSpeed = minNonStopSpeed;
        speedController.set(STOP);
    }


    public SpeedController getSpeedController()
    {
        return controller;
    }


    public LiveWindowSendable getSpeedControllerAsLiveWindowSendable()
    {
        return liveWindowSendable;
    }


    public boolean isInversed()
    {
        return inverse;
    }


    /** @param speed the speed (between -1 and 1) such that a positive speed is forward, a negative speed is reverse, and zero is stopped. */
    public String setSpeed(double speed)
    {
        this.speed = MathUtils.constrainBetweenOneAndNegOne(speed);
        return setSpeedToCurrentSetting();
    }


    /**
     * Sets the speed without modifying the run state such that if the motor is running, its speed with change, but if
     * the motor is stopped, the speed is set for a call to one of the startAtCurrentSpeed methods.
     *
     * @param speed the speed to use
     */
    public void setSpeedWithoutModifyingRunState(double speed)
    {
        this.speed = MathUtils.constrainBetweenOneAndNegOne(speed);
        if (isRunning())
        {
            setSpeedToCurrentSetting();
        }
    }


    public void setMinNonStopSpeed(double minNonStopSpeed)
    {
        this.minNonStopSpeed = MathUtils.constrainBetweenOneAndNegOne(speed);
    }


    protected String setSpeedToCurrentSetting()
    {
        if (Math.abs(speed) < minNonStopSpeed)
        {
            speed = 0;
        }
        speed = MathUtils.constrainBetweenOneAndNegOne(speed);
        controller.set(speed * inverseFactor);
        return getSpeedPercentage();
    }


    /**
     * Makes use of a PID Controller's output by calling the {@link edu.wpi.first.wpilibj.SpeedController#pidWrite(double) pidWrite(double)} method on the SpeedController.
     *
     * @param pidOutput the
     */
    public void pidWrite(double pidOutput)
    {
        controller.pidWrite(pidOutput);
        speed = controller.get();
    }


    public double startAtCurrentSetSpeedAndDirection()
    {
        setSpeedToCurrentSetting();
        return speed;
    }


    public double startForwardAtCurrentRelativeSpeed()
    {
        double neededSpeed = Math.abs(speed);
        setSpeed(neededSpeed);
        return this.speed;
    }


    public double startReverseAtCurrentRelativeSpeed()
    {
        double neededSpeed = -Math.abs(speed);
        setSpeed(neededSpeed);
        return this.speed;
    }


    public double toggleDirection()
    {
        this.speed = -this.speed;
        setSpeedToCurrentSetting();
        return this.speed;
    }


    public String stop()
    {
        //We do not set the speed field to zero as we want to retain the last speed used for a restart.
        controller.set(STOP);
        return getSpeedPercentage();
    }



    public void setForwardSpeed(double speed) { setSpeed(speed); }

    public void setForward05Percent() { setSpeed(0.05); }


    public void setForward10Percent() { setSpeed(0.10); }


    public void setForward15Percent() { setSpeed(0.15); }


    public void setForward20Percent() { setSpeed(0.20); }


    public void setForward25Percent() { setSpeed(0.25); }


    public void setForward30Percent() { setSpeed(0.30); }


    public void setForward35Percent() { setSpeed(0.35); }


    public void setForward40Percent() { setSpeed(0.40); }


    public void setForward45Percent() { setSpeed(0.45); }


    public void setForward50Percent() { setSpeed(0.50); }


    public void setForward55Percent() { setSpeed(0.55); }


    public void setForward60Percent() { setSpeed(0.60); }


    public void setForward65Percent() { setSpeed(0.65); }


    public void setForward70Percent() { setSpeed(0.70); }


    public void setForward75Percent() { setSpeed(0.75); }


    public void setForward80Percent() { setSpeed(0.80); }


    public void setForward85Percent() { setSpeed(0.85); }


    public void setForward90Percent() { setSpeed(0.90); }


    public void setForward95Percent() { setSpeed(0.95); }


    public void setForwardAsPercent(int percentage)
    {
        if (percentage < 0)
        {
            logger.error("Received forward speed is less than 0%. Setting to 0&");
            percentage = 0;
        }
        else if (percentage > 100)
        {
            logger.error("Received forward speed is greater than 100%. Setting to 100%");
            percentage = 100;
        }
        double targetSpeed = percentage / 100;
        setSpeed(targetSpeed);
    }


    public void setForwardFull() { setSpeed(1.0); }


    public void setReverseSpeed(double speed) { setSpeed(-speed); }


    public void setReverse05Percent() { setSpeed(-0.05); }


    public void setReverse10Percent() { setSpeed(-0.10); }


    public void setReverse15Percent() { setSpeed(-0.15); }


    public void setReverse20Percent() { setSpeed(-0.20); }


    public void setReverse25Percent() { setSpeed(-0.25); }


    public void setReverse30Percent() { setSpeed(-0.30); }


    public void setReverse35Percent() { setSpeed(-0.35); }


    public void setReverse40Percent() { setSpeed(-0.40); }


    public void setReverse45Percent() { setSpeed(-0.45); }


    public void setReverse50Percent() { setSpeed(-0.50); }


    public void setReverse55Percent() { setSpeed(-0.55); }


    public void setReverse60Percent() { setSpeed(-0.60); }


    public void setReverse65Percent() { setSpeed(-0.65); }


    public void setReverse70Percent() { setSpeed(-0.70); }


    public void setReverse75Percent() { setSpeed(-0.75); }


    public void setReverse80Percent() { setSpeed(-0.80); }


    public void setReverse85Percent() { setSpeed(-0.85); }


    public void setReverse90Percent() { setSpeed(-0.90); }


    public void setReverse95Percent() { setSpeed(-0.95); }


    public void setReverseAsPercent(int percentage)
    {
        percentage = Math.abs(percentage);
        if (percentage < 0)
        {
            logger.error("Received reverse speed is less than 0%. Setting to 0%");
            percentage = 0;
        }
        else if (percentage > 100)
        {
            logger.error("Received revers speed is greater than 100%. Setting to 100%");
            percentage = 100;
        }
        double targetSpeed = percentage / 100;
        setSpeed(-targetSpeed);
    }


    public void setReverseFull() { setSpeed(-1.0); }


    public String increaseForwardSpeedOnePercent() { return increaseForwardSpeed(.01); }


    public String increaseForwardSpeedThreePercent() { return increaseForwardSpeed(.03); }


    public String increaseForwardSpeedFivePercent() { return increaseForwardSpeed(.05); }


    public String increaseForwardSpeedTenPercent() { return increaseForwardSpeed(.1); }


    public String increaseForwardSpeedFifteenPercent() { return increaseForwardSpeed(.15); }


    public String increaseForwardSpeedTwentyPercent() { return increaseForwardSpeed(.20); }


    public String increaseForwardSpeedTwentyFivePercent() { return increaseForwardSpeed(.25); }


    public String increaseForwardSpeedThirtyPercent() { return increaseForwardSpeed(.30); }


    public String increaseForwardSpeedThirtyFivePercent() { return increaseForwardSpeed(.35); }


    public String increaseForwardSpeedFortyPercent() { return increaseForwardSpeed(.40); }


    public String increaseForwardSpeedFortyFivePercent() { return increaseForwardSpeed(.45); }


    public String increaseForwardSpeedFiftyPercent() { return increaseForwardSpeed(.50); }


    public String increaseForwardSpeedSeventyFivePercent() { return increaseForwardSpeed(.75); }


    public String decreaseForwardSpeedOnePercent() { return decreaseForwardSpeed(.01); }


    public String decreaseForwardSpeedThreePercent() { return decreaseForwardSpeed(.03); }


    public String decreaseForwardSpeedFivePercent() { return decreaseForwardSpeed(.05); }


    public String decreaseForwardSpeedTenPercent() { return decreaseForwardSpeed(.1); }


    public String decreaseForwardSpeedFifteenPercent() { return decreaseForwardSpeed(.15); }


    public String decreaseForwardSpeedTwentyPercent() { return decreaseForwardSpeed(.20); }


    public String decreaseForwardSpeedTwentyFivePercent() { return decreaseForwardSpeed(.25); }


    public String decreaseForwardSpeedThirtyPercent() { return decreaseForwardSpeed(.30); }


    public String decreaseForwardSpeedThirtyFivePercent() { return decreaseForwardSpeed(.35); }


    public String decreaseForwardSpeedFortyPercent() { return decreaseForwardSpeed(.40); }


    public String decreaseForwardSpeedFortyFivePercent() { return decreaseForwardSpeed(.45); }


    public String decreaseForwardSpeedFiftyPercent() { return decreaseForwardSpeed(.50); }


    public String decreaseForwardSpeedSeventyFivePercent() { return decreaseForwardSpeed(.75); }


    /**
     * Increases the forward speed by the specified amount, but does nto allow the motor to go into reverse. For example, if the current speed is 5% (i.e. 0.05) forward and a
     * value
     * of change of 8% (i.e. 0.08) is passed in, the motor speed will be set to 0, not 3% reversed.
     *
     * @param delta the amount to decrease as a value between 0 and 1.
     *
     * @return the new speed
     */
    public String decreaseForwardSpeed(double delta)
    { return modifySpeed(-Math.abs(delta)); }


    public String increaseForwardSpeed(double delta) { return modifySpeed(Math.abs(delta));}


    public boolean isRunning()
    {
        return controller.get() != 0;
    }


    public boolean isStopped()
    {
        return !isRunning();
    }


    public boolean isRunningFullSpeed()
    {
        return Math.abs(controller.get()) >= 1.0D;
    }


    private String modifySpeed(double delta)
    {
        double realDelta = delta * inverseFactor;
        double current = controller.get();
        double newSpeed = current + realDelta;

        speed = MathUtils.constrainBetweenOneAndNegOne(newSpeed);
        return setSpeedToCurrentSetting();
    }


    public double getSpeed()
    {
        return controller.get();
    }


    public double getAbsoluteSpeed()
    {
        return Math.abs(controller.get());
    }


    public String getSpeedPercentage()
    {
        if (controller.get() == 0)
        {
            return "STOPPED";
        }
        return isForward() ? ("F:" + MathUtils.toPercentage(controller.get())) : ("R:" + MathUtils.toPercentage(controller.get()));
    }


    public boolean isForward()
    {
        return inverse ? (controller.get() < 0) : (controller.get() > 0);
    }


    public void updateTable() {
        liveWindowSendable.updateTable(); }


    public void startLiveWindowMode() { liveWindowSendable.startLiveWindowMode(); }


    public void stopLiveWindowMode() { liveWindowSendable.stopLiveWindowMode(); }


    public void initTable(ITable subTable) {
        liveWindowSendable.initTable(subTable); }


    public ITable getTable() {
        return liveWindowSendable.getTable(); }


    public String getSmartDashboardType() {
        return liveWindowSendable.getSmartDashboardType(); }
}